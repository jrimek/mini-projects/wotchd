# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or create!d alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create!([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create!(name: 'Luke', movie: movies.first)
Movie.delete_all
User.delete_all
Actor.delete_all
Genre.delete_all
Country.delete_all
AgeRating.delete_all
MovieTag.delete_all
Season.delete_all


jimbo = User.create!  email: 'jdb@test.de',
                      password: 'test123',
                      password_confirmation: 'test123'

jimbo_movie_list = MovieList.create! name: 'jimbo_movie_list',
                                    user_id: jimbo.id

fsk18 = AgeRating.create! rating: AgeRating.ratings[:fsk18]

fsk16 = AgeRating.create! rating: AgeRating.ratings[:fsk16]

fsk12 = AgeRating.create! rating: AgeRating.ratings[:fsk12]

fsk06 = AgeRating.create! rating: AgeRating.ratings[:fsk6]

fsk0 = AgeRating.create!  rating: AgeRating.ratings[:fsk0]

r = AgeRating.create! rating: AgeRating.ratings[:r]


emilia = Actor.create!  first_name: 'Emilia',
                        last_name: 'Clarke',
                        sex: Actor.sexes[:female],
                        birthdate: '23.10.1986'

little_finger = Actor.create! first_name: 'Aidan',
                              last_name: 'Gillen',
                              sex: Actor.sexes[:male],
                              birthdate: '24.04.1968'

stevo = Actor.create! first_name: 'Steve',
                      last_name: '-O',
                      sex: Actor.sexes[:male],
                      birthdate: '13.06.1974'


schnulze   = MovieTag.create! name: 'Schnulze'
brainless  = MovieTag.create! name: 'hirnlose action'
popcorn    = MovieTag.create! name: 'popcorn Kino'
cry        = MovieTag.create! name: 'zum heulen'
lachen     = MovieTag.create! name: 'totgelacht'
beste      = MovieTag.create! name: 'beste'


drama   = Genre.create! name: 'Drama'
fantasy = Genre.create! name: 'Fantasy'
action  = Genre.create! name: 'Action'
comedy  = Genre.create! name: 'Komödie'
sci_fi  = Genre.create! name: 'SciFi'
romance = Genre.create! name: 'Liebesfilm'


de  = Country.create! name: 'Deutschland'
uk  = Country.create! name: 'UK'
usa = Country.create! name: 'USA'


jackass = Movie.create! ger_title:       'Jackass: The Movie',
                        engl_title:       'Jackass: The Movie',
                        release_date:     '27.02.2002',
                        duration:          87,
                        imdb_rating:       6.6,
                        metascore_rating:  42,
                        subdivision:       Movie.subdivisions[:movie],
                        short_description: 'very short',
                        age_rating_id:     fsk16.id


CastMember.create!  actor_id: stevo.id, movie_id: jackass.id, role: CastMember.roles[:actor]

MovieTagCount.create! movie_id: jackass.id,
                      movie_tag_id: lachen.id,
                      count: 5

MovieTagCount.create! movie_id: jackass.id,
                      movie_tag_id: brainless.id,
                      count: 50

Rating.create title: "funniest shit ever",
              review: "I fuckin laughed my ass off. Much long review, very wow",
              rating: 10,
              user_id: jimbo.id,
              movie_id: jackass.id
Rating.create rating: 5,
              user_id: jimbo.id,
              movie_id: jackass.id
Rating.create rating: 6,
              user_id: jimbo.id,
              movie_id: jackass.id
Rating.create rating: 7,
              user_id: jimbo.id,
              movie_id: jackass.id
Rating.create rating: 8,
              user_id: jimbo.id,
              movie_id: jackass.id
Rating.create rating: 9,
              user_id: jimbo.id,
              movie_id: jackass.id
Rating.create rating: 7,
              user_id: jimbo.id,
              movie_id: jackass.id
Rating.create rating: 5,
              user_id: jimbo.id,
              movie_id: jackass.id

jimbo_movie_list.add_movie jackass


jackass.genres << comedy
jackass.genres << action

jackass.countries << uk


got = Movie.create!  ger_title:        'Game of Thrones',
                     engl_title:       'Game of Thrones',
                     release_date:     '17.04.2011',
                     duration:          50,
                     imdb_rating:       9.5,
                     subdivision:       Movie.subdivisions[:tv_show],
                     age_rating_id:     fsk16.id,
                     long_description:  " Handlung von Game of Thrones

                                        Im Mittelpunkt von Game of Thrones
                                        befinden sich die Intrigen und Machtspiele
                                        im fiktiven Westeros, das sich insgesamt
                                        aus sieben Königreichen zusammensetzt.
                                        Während im Norden eine gigantische Eiswall
                                        das Land vor außenstehenden Gefahren
                                        beschützt, grenzen Meere des Rest des
                                        Kontinents von anderen Ländern und Kulturen
                                        ab. Der Rahmen für die epische Erzählung
                                        ist sozusagen perfekt abgesteckt; fortan
                                        dreht sich alles um den Kampf hinsichtlich
                                        der Kontrolle über den Eisernen Thron.

                                        Jede der dominierenden Herrscherfamilie
                                        erhebt Anspruch auf den Sitz der Macht in
                                        King's Landing und folglich steht das gezielte
                                        Blutvergießen ganz oben auf der Tagesordnung.
                                        Ob es dabei zur großen Schlacht kommt oder
                                        ein Dolch in den Rücken ausreicht, bleibt den
                                        Drahtziehern des verheerenden Schachspiels auf
                                        Leben und Tod überlassen. Wichtig ist, seine
                                        Verbündeten und Vertrauten besser zu kennen
                                        als diese sich selbst - immerhin kann bereits
                                        die kleinste Unachtsamkeit dazu führen, dass
                                        ein König gestürzt und sein gesamtes Geschlecht
                                        ausgelöscht wird.

                                        Unter diesen düsteren Vorzeichen setzt die
                                        Handlung von Game of Thrones mit dem Schicksal
                                        von Lord Eddard ~Ned~ Stark ein, der aus Winterfall
                                        stammt und der beste Freund von König Robert Baratheon
                                        ist. Eine Tages bittet ihn dieser, sich auf den
                                        Weg in die Hauptstadt zu machen, um dort als Hand
                                        des Königs - sprich als erster Berater - zu fungieren.
                                        Kaum ist Ned in King's Landing angekommen, erwecken
                                        die Umstände des Todes seines Vorgänger sein Misstrauen.",

                     short_description: "Game of Thrones ist eine US-amerikanische
                                        Dramaserie aus dem Hause HBO. Die Geschichte
                                        basiert auf George R.R. Martins Romanreihe A
                                        Song of Ice and Fire, die hierzulande unter
                                        dem Titel Das Lied von Eis und Feuer geläufig
                                        ist. Angesiedelt in den sieben Königreichen
                                        von Westeros folgt Game of Thrones den noblen
                                        Herrscherfamilien, die um die Kontrolle über
                                        den Eisernen Thron kämpfen und darüber hinaus
                                        gemeinsam gegen böse Mächte aus dem eisigen
                                        Norden antreten müssen."


Rating.create rating: 10,
              user_id: jimbo.id,
              movie_id: got.id

CastMember.create! actor_id: emilia.id, movie_id: got.id, role: CastMember.roles[:actor]
CastMember.create! actor_id: little_finger.id, movie_id: got.id, role: CastMember.roles[:actor]

MovieTagCount.create! movie_id: got.id,
                      movie_tag_id: beste.id,
                      count: 50000

MovieTagCount.create! movie_id: got.id,
                      movie_tag_id: popcorn.id,
                      count: 3000


got.genres << drama
got.genres << fantasy

got.countries << uk
got.countries << usa


#got_season_1 = Season.create! name: 'Game of Thrones Season 1',
#                              number: 1,
#                              movie_id: got.id
#
#got1_1 = Movie.create!  ger_title:        'Winter ist coming',
#                        engl_title:       'Winter ist coming',
#                        release_date:     '17.04.2011',
#                        duration:          62,
#                        imdb_rating:       9,
#                        subdivision:       Movie.subdivisions[:episode],
#                        season_id:         got_season_1.id,
#                        short_description: 'very short',
#                        age_rating_id:     fsk16.id
#
#
#CastMember.create! actor_id: emilia.id, movie_id: got1_1.id, role: CastMember.roles[:actor]
#CastMember.create! actor_id: little_finger.id, movie_id: got1_1.id, role: CastMember.roles[:actor]
#
#
#MovieTagCount.create! movie_id: got1_1.id,
#                      movie_tag_id: beste.id,
#                      count: 500
#
#MovieTagCount.create! movie_id: got1_1.id,
#                      movie_tag_id: popcorn.id,
#                      count: 30
#
#
#got1_1.genres << drama
#got1_1.genres << fantasy
#
#got1_1.countries << uk
#got1_1.countries << usa
#
#
#got1_2 = Movie.create!  ger_title:        'The Kingsroad',
#                        engl_title:       'The Kingsroad',
#                        release_date:     '17.04.2011',
#                        duration:          62,
#                        imdb_rating:       9,
#                        subdivision:       Movie.subdivisions[:episode],
#                        season_id:         1,
#                        short_description: 'very short',
#                        age_rating_id:     fsk16.id
#
#
#CastMember.create! actor_id: emilia.id, movie_id: got1_2.id, role: CastMember.roles[:actor]
#CastMember.create! actor_id: little_finger.id, movie_id: got1_2.id, role: CastMember.roles[:actor]
#
#
#MovieTagCount.create! movie_id: got1_2.id,
#                      movie_tag_id: beste.id,
#                      count: 500
#
#MovieTagCount.create! movie_id: got1_2.id,
#                      movie_tag_id: popcorn.id,
#                      count: 30
#
#
#got1_2.genres << drama
#got1_2.genres << fantasy
#
#got1_2.countries << uk
#got1_2.countries << usa
#
10.times do |i|
  Country.create! name: "country_#{i}"
end

30.times do |i|
  Genre.create! name: "genre_#{i}"
end

10.times do |i| #inc?
  MovieTag.create! name: "movie_tag_#{i}"
end

100.times do |i| #1000
  Actor.create! first_name: "first_name_#{i}",
                last_name: "last_name_#{i}",
                sex: rand(9) > 5 ? Actor.sexes[:female] : Actor.sexes[:male],
                birthdate: rand(30000).days.ago
end

100.times do |i| #creating users takes ages, for 50.000 it took me well over
                   #1h, best guess is because of the encryption of the password
  User.create! email: "seed_#{i}@test.de",
               password: "password_#{i}",
               password_confirmation: "password_#{i}"
end
#actual movies
200.times do |i| #10.000
  Movie.create! engl_title: "engl_title_#{i}",
                release_date: rand(50000).days.ago,
                duration: rand(240),
                subdivision: Movie.subdivisions[:movie],
                short_description: 'very short',
                age_rating_id:     fsk16.id
end

countries_count = Country.all.count
genres_count = Genre.all.count
movie_tags_count =MovieTag.all.count
actor_count = Actor.all.count
user_count = User.count

Movie.all.each do |movie|
  # there can't be duplicate movie_id countrie_id pairs so I just create 
  # one country/genre. rand(1) is always 0
  (rand(1)+1).times do |i|
    movie.countries << Country.find(rand(countries_count)+1)
  end

  (rand(1)+1).times do |i| #see above
    movie.genres << Genre.find(rand(genres_count)+1)
  end

  rand(100).times do |i|
    movie.movie_tags << MovieTag.find(rand(movie_tags_count)+1)
  end

  (rand(3)+1).times do |i|
    movie.add_actor_with_role( Actor.find(rand(actor_count)+1),
                                          CastMember.roles[:actor])
  end


  user_count.times do |i|
    Rating.create rating: rand(10),
                  user_id: i + 1,
                  movie_id: movie.id
  end

end
