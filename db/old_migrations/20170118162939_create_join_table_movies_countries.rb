class CreateJoinTableMoviesCountries < ActiveRecord::Migration[5.0]
  def change
    create_join_table :movies, :countries do |t|
       t.index [:movie_id, :country_id]
       t.index [:country_id, :movie_id]
    end
  end
end
