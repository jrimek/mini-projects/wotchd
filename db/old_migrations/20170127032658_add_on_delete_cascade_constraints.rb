class AddOnDeleteCascadeConstraints < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      ALTER TABLE movie_tag_counts
        ADD CONSTRAINT fk_movie_tag_counts_movie_id_on_delete_cascade
        FOREIGN KEY (movie_id)
        REFERENCES movies (id)
        ON DELETE CASCADE;

      ALTER TABLE descriptions
        ADD CONSTRAINT fk_descriptions_movie_id_on_delete_cascade
        FOREIGN KEY (movie_id)
        REFERENCES movies (id)
        ON DELETE CASCADE;

      ALTER TABLE ratings
        ADD CONSTRAINT fk_ratings_movie_id_on_delete_cascade
        FOREIGN KEY (movie_id)
        REFERENCES movies (id)
        ON DELETE CASCADE;

      ALTER TABLE seasons
        ADD CONSTRAINT fk_seasons_movie_id_on_delete_cascade
        FOREIGN KEY (movie_id)
        REFERENCES movies (id)
        ON DELETE CASCADE;

      ALTER TABLE cast_crews
        ADD CONSTRAINT fk_cast_crews_movie_id_on_delete_cascade
        FOREIGN KEY (movie_id)
        REFERENCES movies (id)
        ON DELETE CASCADE;

-----------------------------------

      ALTER TABLE movie_tag_counts
        ADD CONSTRAINT fk_movie_tag_counts_movie_tag_id_on_delete_cascade
        FOREIGN KEY (movie_tag_id)
        REFERENCES movie_tags (id)
        ON DELETE CASCADE;

      ALTER TABLE descriptions
        ADD CONSTRAINT fk_descriptions_cast_crew_member_id_on_delete_cascade
        FOREIGN KEY (cast_crew_member_id)
        REFERENCES cast_crew_members (id)
        ON DELETE CASCADE;

      ALTER TABLE descriptions
        ADD CONSTRAINT fk_descriptions_user_id_on_delete_restrict
        FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE RESTRICT;

      ALTER TABLE ratings
        ADD CONSTRAINT fk_ratings_user_id_on_delete_restrict
        FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE RESTRICT;

      ALTER TABLE movies
        ADD CONSTRAINT fk_movies_season_id_on_delete_cascade
        FOREIGN KEY (season_id)
        REFERENCES seasons (id)
        ON DELETE CASCADE;

      ALTER TABLE lists
        ADD CONSTRAINT fk_lists_user_id_on_delete_cascade
        FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE;
    SQL
    #fk_#{tablename}_#{column}_on_delete_cascade
  end

  def down
    execute <<-SQL
      ALTER TABLE movie_tag_counts
        DROP CONSTRAINT fk_movie_tag_counts_movie_id_on_delete_cascade;

      ALTER TABLE descriptions
        DROP CONSTRAINT fk_descriptions_movie_id_on_delete_cascade;

      ALTER TABLE ratings
        DROP CONSTRAINT fk_ratings_movie_id_on_delete_cascade;

      ALTER TABLE seasons
        DROP CONSTRAINT fk_seasons_movie_id_on_delete_cascade;

      ALTER TABLE cast_crews
        DROP CONSTRAINT fk_cast_crews_movie_id_on_delete_cascade;
        ------------------------------------------------------------------

      ALTER TABLE movie_tag_counts
        DROP CONSTRAINT fk_movie_tag_counts_movie_tag_id_on_delete_cascade;

      ALTER TABLE descriptions
        DROP CONSTRAINT fk_descriptions_cast_crew_member_id_on_delete_cascade;

      ALTER TABLE descriptions
        DROP CONSTRAINT fk_descriptions_user_id_on_delete_cascade;

      ALTER TABLE ratings
        DROP CONSTRAINT fk_ratings_user_id_on_delete_cascade;

      ALTER TABLE movies
        DROP CONSTRAINT fk_movies_season_id_on_delete_cascade;

      ALTER TABLE lists
        DROP CONSTRAINT fk_lists_user_id_on_delete_cascade;


    SQL
  end
end
