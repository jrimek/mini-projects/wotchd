class AddMovieTypesToMovies < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      CREATE TYPE movie_subdivision AS ENUM ('Movie', 'TV Show', 'Episode');

      ALTER TABLE movies
        ADD COLUMN subdivision movie_subdivision;

    SQL

    add_index :movies, :subdivision
  end

  def down
    remove_column :movies, :subdivision

    execute <<-SQL
      DROP TYPE movie_subdivision;
    SQL
  end
end
