class AddColumnsToAgeRating < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      CREATE TYPE age_rating AS ENUM ('FSK 0','FSK 6','FSK 12','FSK 16','FSK 18',
                                      'PG','PG-13','R','NC-17');

      CREATE TYPE age_rating_region AS ENUM ('DE', 'USA');

      ALTER TABLE age_ratings
        ADD COLUMN rating age_rating,
        ADD COLUMN region age_rating_region;

    SQL

    add_index :age_ratings, :rating
  end

  def down
    remove_column :age_ratings, :rating
    remove_column :age_ratings, :region

    execute <<-SQL
      DROP TYPE age_rating;
      DROP TYPE age_rating_region;
    SQL
  end

end
