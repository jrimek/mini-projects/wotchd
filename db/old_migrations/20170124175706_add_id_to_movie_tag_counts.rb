class AddIdToMovieTagCounts < ActiveRecord::Migration[5.0]
  def change
    add_column :movie_tag_counts, :id, :primary_key
    add_column :cast_crews, :id, :primary_key
  end
end
