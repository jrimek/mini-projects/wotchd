class AddFkToJoinTablesWithOnDeleteCascade < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
    ALTER TABLE countries_movies
      ADD CONSTRAINT fk_countries_movies_movie_id_on_delete_cascade
      FOREIGN KEY (movie_id)
      REFERENCES movies (id)
      ON DELETE CASCADE;

    ALTER TABLE genres_movies
      ADD CONSTRAINT fk_genres_movies_movie_id_on_delete_cascade
      FOREIGN KEY (movie_id)
      REFERENCES movies (id)
      ON DELETE CASCADE;

    ALTER TABLE age_ratings_movies
      ADD CONSTRAINT fk_age_ratings_movies_movie_id_on_delete_cascade
      FOREIGN KEY (movie_id)
      REFERENCES movies (id)
      ON DELETE CASCADE;

    ALTER TABLE lists_movies
      ADD CONSTRAINT fk_lists_movies_movie_id_on_delete_cascade
      FOREIGN KEY (movie_id)
      REFERENCES movies (id)
      ON DELETE CASCADE;

------------------------------------------------------------------------

    ALTER TABLE countries_movies
      ADD CONSTRAINT fk_countries_movies_country_id_on_delete_cascade
      FOREIGN KEY (country_id)
      REFERENCES countries (id)
      ON DELETE CASCADE;

    ALTER TABLE genres_movies
      ADD CONSTRAINT fk_genres_movies_genre_id_on_delete_cascade
      FOREIGN KEY (genre_id)
      REFERENCES genres (id)
      ON DELETE CASCADE;

    ALTER TABLE age_ratings_movies
      ADD CONSTRAINT fk_age_ratings_movies_age_rating_id_on_delete_cascade
      FOREIGN KEY (age_rating_id)
      REFERENCES age_ratings (id)
      ON DELETE CASCADE;

      ALTER TABLE lists_movies
        ADD CONSTRAINT fk_lists_movies_list_id_on_delete_cascade
        FOREIGN KEY (list_id)
        REFERENCES lists (id)
        ON DELETE CASCADE;

    SQL
  end

  def down
    execute <<-SQL
    ALTER TABLE countries_movies
      DROP CONSTRAINT fk_countries_movies_movie_id_on_delete_cascade;

    ALTER TABLE genres_movies
      DROP CONSTRAINT fk_genres_movies_movie_id_on_delete_cascade;

    ALTER TABLE age_ratings_movies
      DROP CONSTRAINT fk_age_ratings_movies_movie_id_on_delete_cascade;

    ALTER TABLE countries_movies
      DROP CONSTRAINT fk_countries_movies_country_id_on_delete_cascade;

    ALTER TABLE genres_movies
      DROP CONSTRAINT fk_genres_movies_genre_id_on_delete_cascade;

    ALTER TABLE age_ratings_movies
      DROP CONSTRAINT fk_age_ratings_movies_age_rating_id_on_delete_cascade;


    SQL
  end
end
