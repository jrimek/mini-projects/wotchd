class AddNotNullContraintsToSeasons < ActiveRecord::Migration[5.0]
  def change
    change_column_null :seasons, :name, false
    change_column_null :seasons, :number, false
    change_column_null :cast_crew_members, :sex, false
    change_column_null :cast_crew_members, :name, false
    change_column_null :cast_crews, :role, false
    change_column_null :movie_tags, :name, false
    change_column_null :movie_tag_counts, :count, false
    change_column_null :genres, :name, false
    change_column_null :countries, :name, false
    change_column_null :movies, :duration, false
    change_column_null :movies, :subdivision, false
    change_column_null :movies, :release_date, false
    change_column_null :movies, :engl_title, false
    change_column_null :age_ratings, :region, false
    change_column_null :age_ratings, :rating, false
  end
end
