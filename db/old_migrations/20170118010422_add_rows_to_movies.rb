class AddRowsToMovies < ActiveRecord::Migration[5.0]
  def change
    add_column :movies, :metascore_rating, :integer
  end
end
