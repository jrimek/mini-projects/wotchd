class CreateCastCrewMembers < ActiveRecord::Migration[5.0]
  def change
    create_table :cast_crew_members do |t|
      t.string :name
      t.date :birthdate

      t.timestamps
    end
  end
end
