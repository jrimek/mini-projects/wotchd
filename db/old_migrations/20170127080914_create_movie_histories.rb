class CreateMovieHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :movie_histories do |t|
      t.integer :movie_id, null: false
      t.json :movie_content, null: false

      t.timestamps
    end
  end
end
