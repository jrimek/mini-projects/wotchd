class AddStatusAndFkToDescriptionHistory < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      ALTER TABLE description_histories
        ADD CONSTRAINT fk_description_histories_description_id_on_delete_cascade
        FOREIGN KEY (description_id)
        REFERENCES descriptions (id)
        ON DELETE CASCADE;

      CREATE TYPE history_status AS ENUM ('declined', 'current', 'accepted', 'pending');

      ALTER TABLE description_histories
        ADD COLUMN status history_status NOT NULL;

    SQL
  end
  def down
    remove_column :description_histories, :status

    execute <<-SQL
      ALTER TABLE description_histories
        DROP CONSTRAINT fk_description_histories_description_id_on_delete_cascade;

      DROP TYPE history_status;
    SQL
  end
end
