class CreateActorLists < ActiveRecord::Migration[5.0]
  def change
    create_table :actor_lists do |t|
      t.string :name, null: false
      t.boolean :fan_list, null: false
      t.integer :user_id, null: false

      t.timestamps
    end
  end
end
