class AddVarCharMaxLength < ActiveRecord::Migration[5.0]
  def change
    change_column :users, :nickname, :string, limit: 30
    change_column :lists, :name, :string, limit: 50
    change_column :seasons, :name, :string, limit: 50
    change_column :movie_tags, :name, :string, limit: 50
    change_column :genres, :name, :string, limit: 50
    change_column :movies, :engl_title, :string, limit: 100
    change_column :movies, :ger_title, :string, limit: 100
  end
end
