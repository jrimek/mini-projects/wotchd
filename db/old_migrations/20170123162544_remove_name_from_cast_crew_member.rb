class RemoveNameFromCastCrewMember < ActiveRecord::Migration[5.0]
  def change
    remove_column :cast_crew_members, :name
    change_column_null :cast_crew_members, :first_name, false
    change_column_null :cast_crew_members, :last_name, false
  end
end
