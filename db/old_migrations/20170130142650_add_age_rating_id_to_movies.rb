class AddAgeRatingIdToMovies < ActiveRecord::Migration[5.0]
  def up
    add_column :movies, :age_rating_id, :integer, null: false


    execute <<-SQL
      ALTER TABLE movies
        ADD CONSTRAINT fk_movies_age_rating_id_on_delete_cascade
        FOREIGN KEY (age_rating_id)
        REFERENCES age_ratings (id)
        ON DELETE CASCADE;
    SQL
  end
  def down
    execute <<-SQL
      ALTER TABLE movies
        DROP CONSTRAINT fk_movies_age_rating_id_on_delete_cascade;
    SQL
    remove_column :movies, :age_rating_id
  end
end
