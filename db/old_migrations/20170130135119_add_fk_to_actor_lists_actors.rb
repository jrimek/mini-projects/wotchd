class AddFkToActorListsActors < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      ALTER TABLE actor_lists_actors
        ADD CONSTRAINT fk_actor_lists_actors_actor_id_on_delete_cascade
        FOREIGN KEY (actor_id)
        REFERENCES actors (id)
        ON DELETE CASCADE;

      ALTER TABLE actor_lists_actors
        ADD CONSTRAINT fk_actor_lists_actors_actor_list_id_on_delete_cascade
        FOREIGN KEY (actor_list_id)
        REFERENCES actor_lists (id)
        ON DELETE CASCADE;
    SQL
  end
  def down
    execute <<-SQL
      ALTER TABLE actor_lists_actors
        DROP CONSTRAINT fk_actor_lists_actors_actor_id_on_delete_cascade;
      ALTER TABLE actor_lists_actors
        DROP CONSTRAINT fk_actor_lists_actors_actor_list_id_on_delete_cascade;
    SQL
  end
end
