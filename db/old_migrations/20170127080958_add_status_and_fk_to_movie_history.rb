class AddStatusAndFkToMovieHistory < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      ALTER TABLE movie_histories
        ADD CONSTRAINT fk_movie_histories_movie_id_on_delete_cascade
        FOREIGN KEY (movie_id)
        REFERENCES movies (id)
        ON DELETE CASCADE;

      ALTER TABLE movie_histories
        ADD COLUMN status history_status NOT NULL;

    SQL
  end
  def down
    remove_column :movie_histories, :status

    execute <<-SQL
      ALTER TABLE movie_histories
        DROP CONSTRAINT fk_movie_histories_movie_id_on_delete_cascade;

    SQL
  end
end
