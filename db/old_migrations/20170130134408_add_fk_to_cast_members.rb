class AddFkToCastMembers < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      ALTER TABLE cast_members
        ADD CONSTRAINT fk_cast_members_actor_id_on_delete_cascade
        FOREIGN KEY (actor_id)
        REFERENCES actors (id)
        ON DELETE CASCADE;

    SQL
  end
  def down
    execute <<-SQL
      ALTER TABLE actor_histories
        DROP CONSTRAINT fk_cast_members_actor_id_on_delete_cascade;
    SQL
  end
end
