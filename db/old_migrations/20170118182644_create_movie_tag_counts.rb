class CreateMovieTagCounts < ActiveRecord::Migration[5.0]
  def change
    create_table :movie_tag_counts, :id => false do |t|
      t.references :movie, foreign_key: true
      t.references :movie_tag, foreign_key: true
      t.integer :count

      t.timestamps
    end
  end
end
