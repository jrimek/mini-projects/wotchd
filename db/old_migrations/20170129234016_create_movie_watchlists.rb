class CreateMovieWatchlists < ActiveRecord::Migration[5.0]
  def change
    create_table :movie_watchlists do |t|
      t.integer :movie_id, null: false
      t.integer :user_id, null: false

      t.timestamps
    end
  end
end
