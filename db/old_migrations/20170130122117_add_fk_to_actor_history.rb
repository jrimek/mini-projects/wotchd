class AddFkToActorHistory < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      ALTER TABLE actor_histories
        ADD CONSTRAINT fk_actor_histories_actor_id_on_delete_cascade
        FOREIGN KEY (actor_id)
        REFERENCES actors (id)
        ON DELETE CASCADE;

      ALTER TABLE actor_histories
        ADD COLUMN status history_status NOT NULL;

    SQL
  end
  def down
    remove_column :actor_histories, :status

    execute <<-SQL
      ALTER TABLE actor_histories
        DROP CONSTRAINT fk_movie_histories_actor_id_on_delete_cascade;

    SQL
  end
end
