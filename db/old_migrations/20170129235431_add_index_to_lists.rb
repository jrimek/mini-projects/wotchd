class AddIndexToLists < ActiveRecord::Migration[5.0]
  def change
    add_index :actor_fanlists, [:user_id, :actor_id]
    add_index :actor_fanlists, [:actor_id, :user_id]
    add_index :movie_watchlists, [:movie_id, :user_id]
    add_index :movie_watchlists, [:user_id, :movie_id]
  end
end
