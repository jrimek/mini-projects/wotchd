class AddFkConstraint2 < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      ALTER TABLE actor_fanlists
        ADD CONSTRAINT fk_actor_fanlists_user_id_on_delete_cascade
        FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE;

      ALTER TABLE actor_fanlists
        ADD CONSTRAINT fk_actor_fanlists_actor_id_on_delete_cascade
        FOREIGN KEY (actor_id)
        REFERENCES actors (id)
        ON DELETE CASCADE;

      ALTER TABLE movie_watchlists
        ADD CONSTRAINT fk_movie_watchlists_user_id_on_delete_cascade
        FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE;

      ALTER TABLE movie_watchlists
        ADD CONSTRAINT fk_movie_watchlists_movie_id_on_delete_cascade
        FOREIGN KEY (movie_id)
        REFERENCES movies (id)
        ON DELETE CASCADE;

      SQL
  end

  def down
  end
end
