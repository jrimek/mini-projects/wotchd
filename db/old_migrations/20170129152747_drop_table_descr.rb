class DropTableDescr < ActiveRecord::Migration[5.0]
  def change
    drop_table :description_histories
    drop_table :descriptions
  end
end
