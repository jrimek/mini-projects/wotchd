class RenameCastCrewMembers < ActiveRecord::Migration[5.0]
  def change
    rename_table :cast_crew_members, :actors
    rename_table :cast_crews, :cast_members
    rename_column :cast_members, :cast_crew_member_id, :actor_id
  end
end
