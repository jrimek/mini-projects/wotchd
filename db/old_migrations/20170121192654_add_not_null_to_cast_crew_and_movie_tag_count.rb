class AddNotNullToCastCrewAndMovieTagCount < ActiveRecord::Migration[5.0]
  def change
    change_column_null :movie_tag_counts, :movie_id, false
    change_column_null :movie_tag_counts, :movie_tag_id, false
    change_column_null :cast_crews, :movie_id, false
    change_column_null :cast_crews, :cast_crew_member_id, false
  end
end
