class AddDescription < ActiveRecord::Migration[5.0]
  def change
    add_column :movies, :short_description, :string, null: false, default: 'short description', limit: 1000
    add_column :movies, :long_description, :string
    rename_table :lists, :movie_lists
  end
end
