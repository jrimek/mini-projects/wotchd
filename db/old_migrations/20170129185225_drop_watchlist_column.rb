class DropWatchlistColumn < ActiveRecord::Migration[5.0]
  def change
    remove_column :movie_lists, :watchlist
  end
end
