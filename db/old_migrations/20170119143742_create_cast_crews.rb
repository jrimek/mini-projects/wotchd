class CreateCastCrews < ActiveRecord::Migration[5.0]
  def change
    create_table :cast_crews, :id => false do |t|
      t.references :movie, foreign_key: true
      t.references :cast_crew_member, foreign_key: true

      t.timestamps
    end
  end
end
