class RemoveFanlistFromActorLists < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      ALTER TABLE actor_lists
        ADD CONSTRAINT fk_actor_lists_user_id_on_delete_cascade
        FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE;
    SQL
    remove_column :actor_lists, :fan_list
  end
  def down
    execute <<-SQL
      ALTER TABLE actor_lists
        DROP CONSTRAINT fk_movie_histories_user_id_on_delete_cascade;

    SQL
  end
end
