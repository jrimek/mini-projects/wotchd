class CreateMovies < ActiveRecord::Migration[5.0]
  def change
    create_table :movies do |t|
      t.string :ger_title
      t.string :engl_title
      t.date :realease_date
      t.integer :duration
      t.decimal :imdb_rating, precision: 2, scale: 1

      t.timestamps
    end
  end
end
