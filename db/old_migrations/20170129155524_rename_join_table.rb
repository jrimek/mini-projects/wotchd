class RenameJoinTable < ActiveRecord::Migration[5.0]
  def change
    rename_table :lists_movies, :movie_lists_movies
    rename_column :movie_lists_movies, :list_id, :movie_list_id
  end
end
