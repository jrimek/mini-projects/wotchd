class AddRoleToCastCrew < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      CREATE TYPE cast_crew_role AS ENUM ('Schauspieler', 'Regisseur', 'Produzent', 'Drehbuchautor');

      ALTER TABLE cast_crews
        ADD COLUMN role cast_crew_role;

    SQL

    add_index :cast_crews, :role
  end

  def down
    remove_column :cast_crews, :role

    execute <<-SQL
      DROP TYPE cast_crew_role;
    SQL
  end
end
