class CreateDescriptions < ActiveRecord::Migration[5.0]
  def change
    create_table :descriptions do |t|
      t.text :text, null: false
      t.integer :update_threshold
      t.boolean :accepted, default: false
      t.boolean :short, default: false
      t.references :user, foreign_key: true, null: false
      t.references :movie, foreign_key: true
      t.references :cast_crew_member, foreign_key: true
      t.references :description, foreign_key: true

      t.timestamps
    end
  end
end
