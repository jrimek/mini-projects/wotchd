class CreateRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :ratings do |t|
      t.string :title, limit: 100
      t.text :review
      t.decimal :rating, precision: 3, scale: 1, null: false
      t.references :user, foreign_key: true, null: false
      t.references :movie, foreign_key: true, null: false

      t.timestamps
    end
  end
end
