class CreateSeasons < ActiveRecord::Migration[5.0]
  def change
    create_table :seasons do |t|
      t.integer :number
      t.string :name
      t.date :release_date

      t.timestamps
    end
  end
end
