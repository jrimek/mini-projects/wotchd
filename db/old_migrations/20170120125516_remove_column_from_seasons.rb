class RemoveColumnFromSeasons < ActiveRecord::Migration[5.0]
  def change
    remove_column :seasons, :release_date, :string
  end
end
