class AddSexToCastCrewMember < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      CREATE TYPE sex AS ENUM ('Mann', 'Frau');

      ALTER TABLE cast_crew_members
        ADD COLUMN sex sex;

    SQL
  end

  def down
    remove_column :cast_crew_members, :sex

    execute <<-SQL
      DROP TYPE sex;
    SQL
  end
end
