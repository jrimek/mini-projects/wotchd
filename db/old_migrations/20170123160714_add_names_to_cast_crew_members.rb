class AddNamesToCastCrewMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :cast_crew_members, :first_name, :string, limit: 50
    add_column :cast_crew_members, :last_name, :string, limit: 50
    add_column :cast_crew_members, :middle_name, :string, limit: 50
    change_column_null :cast_crew_members, :name, true
  end
end
