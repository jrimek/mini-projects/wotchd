class DeleteAgeRatingsMovies < ActiveRecord::Migration[5.0]
  def change
    drop_table :age_ratings_movies
    remove_column :age_ratings, :region

    execute <<-SQL
      DROP TYPE age_rating_region;
    SQL
  end
end
