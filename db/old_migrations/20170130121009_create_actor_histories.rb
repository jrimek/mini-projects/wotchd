class CreateActorHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :actor_histories do |t|
      t.integer :actor_id, null: false
      t.json :actor_content, null: false

      t.timestamps
    end

    add_index :movie_histories, :movie_id
    add_index :actor_histories, :actor_id
  end
end
