class AddSeasonIdToMovies < ActiveRecord::Migration[5.0]
  def change
    add_reference :movies, :season, foreign_key: true
    add_reference :seasons, :movie, foreign_key: true
  end
end
