class AddUniqueConstraintOnTwoColumns < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
    ALTER TABLE cast_members
      ADD UNIQUE (movie_id, actor_id, role);

    ALTER TABLE actor_lists_actors
      ADD UNIQUE (actor_list_id, actor_id);

    ALTER TABLE actor_fanlists
      ADD UNIQUE (user_id, actor_id);

    ALTER TABLE ratings
      ADD UNIQUE (movie_id, user_id);

    ALTER TABLE movie_watchlists
      ADD UNIQUE (movie_id, user_id);

    ALTER TABLE movie_lists_movies
      ADD UNIQUE (movie_id, movie_list_id);

    ALTER TABLE movie_tag_counts
      ADD UNIQUE (movie_id, movie_tag_id);

    ALTER TABLE genres_movies
      ADD UNIQUE (movie_id, genre_id);

    ALTER TABLE countries_movies
      ADD UNIQUE (movie_id, country_id);
    SQL
  end
  def down
  end
end
