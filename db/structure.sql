--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: age_rating; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE age_rating AS ENUM (
    'FSK 0',
    'FSK 6',
    'FSK 12',
    'FSK 16',
    'FSK 18',
    'PG',
    'PG-13',
    'R',
    'NC-17'
);


--
-- Name: cast_crew_role; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE cast_crew_role AS ENUM (
    'Schauspieler',
    'Regisseur',
    'Produzent',
    'Drehbuchautor'
);


--
-- Name: history_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE history_status AS ENUM (
    'declined',
    'current',
    'accepted',
    'pending'
);


--
-- Name: movie_subdivision; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE movie_subdivision AS ENUM (
    'Movie',
    'TV Show',
    'Episode'
);


--
-- Name: sex; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE sex AS ENUM (
    'Mann',
    'Frau'
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: actor_fanlists; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE actor_fanlists (
    id integer NOT NULL,
    actor_id integer NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: actor_fanlists_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE actor_fanlists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: actor_fanlists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE actor_fanlists_id_seq OWNED BY actor_fanlists.id;


--
-- Name: actor_histories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE actor_histories (
    id integer NOT NULL,
    actor_id integer NOT NULL,
    actor_content json NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    status history_status NOT NULL
);


--
-- Name: actor_histories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE actor_histories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: actor_histories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE actor_histories_id_seq OWNED BY actor_histories.id;


--
-- Name: actor_lists; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE actor_lists (
    id integer NOT NULL,
    name character varying NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: actor_lists_actors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE actor_lists_actors (
    actor_list_id integer NOT NULL,
    actor_id integer NOT NULL
);


--
-- Name: actor_lists_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE actor_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: actor_lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE actor_lists_id_seq OWNED BY actor_lists.id;


--
-- Name: actors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE actors (
    id integer NOT NULL,
    birthdate date,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    sex sex NOT NULL,
    first_name character varying(50) NOT NULL,
    last_name character varying(50) NOT NULL,
    middle_name character varying(50)
);


--
-- Name: actors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE actors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: actors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE actors_id_seq OWNED BY actors.id;


--
-- Name: age_ratings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE age_ratings (
    id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    rating age_rating NOT NULL
);


--
-- Name: age_ratings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE age_ratings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: age_ratings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE age_ratings_id_seq OWNED BY age_ratings.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: cast_members; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cast_members (
    movie_id integer NOT NULL,
    actor_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    role cast_crew_role NOT NULL,
    id integer NOT NULL
);


--
-- Name: cast_members_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cast_members_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cast_members_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cast_members_id_seq OWNED BY cast_members.id;


--
-- Name: countries; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE countries (
    id integer NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: countries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: countries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE countries_id_seq OWNED BY countries.id;


--
-- Name: countries_movies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE countries_movies (
    movie_id integer NOT NULL,
    country_id integer NOT NULL
);


--
-- Name: genres; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE genres (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: genres_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE genres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: genres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE genres_id_seq OWNED BY genres.id;


--
-- Name: genres_movies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE genres_movies (
    movie_id integer NOT NULL,
    genre_id integer NOT NULL
);


--
-- Name: movie_histories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE movie_histories (
    id integer NOT NULL,
    movie_id integer NOT NULL,
    movie_content json NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    status history_status NOT NULL
);


--
-- Name: movie_histories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE movie_histories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: movie_histories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE movie_histories_id_seq OWNED BY movie_histories.id;


--
-- Name: movie_lists; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE movie_lists (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: movie_lists_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE movie_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: movie_lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE movie_lists_id_seq OWNED BY movie_lists.id;


--
-- Name: movie_lists_movies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE movie_lists_movies (
    movie_list_id integer NOT NULL,
    movie_id integer NOT NULL
);


--
-- Name: movie_tag_counts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE movie_tag_counts (
    movie_id integer NOT NULL,
    movie_tag_id integer NOT NULL,
    count integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    id integer NOT NULL
);


--
-- Name: movie_tag_counts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE movie_tag_counts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: movie_tag_counts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE movie_tag_counts_id_seq OWNED BY movie_tag_counts.id;


--
-- Name: movie_tags; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE movie_tags (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: movie_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE movie_tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: movie_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE movie_tags_id_seq OWNED BY movie_tags.id;


--
-- Name: movie_watchlists; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE movie_watchlists (
    id integer NOT NULL,
    movie_id integer NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: movie_watchlists_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE movie_watchlists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: movie_watchlists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE movie_watchlists_id_seq OWNED BY movie_watchlists.id;


--
-- Name: movies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE movies (
    id integer NOT NULL,
    ger_title character varying(100),
    engl_title character varying(100) NOT NULL,
    release_date date NOT NULL,
    duration integer NOT NULL,
    imdb_rating numeric(2,1),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    metascore_rating integer,
    subdivision movie_subdivision NOT NULL,
    season_id integer,
    short_description character varying(1000) DEFAULT 'short description'::character varying NOT NULL,
    long_description character varying,
    age_rating_id integer NOT NULL
);


--
-- Name: movies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE movies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: movies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE movies_id_seq OWNED BY movies.id;


--
-- Name: ratings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ratings (
    id integer NOT NULL,
    title character varying(100),
    review text,
    rating numeric(3,1) NOT NULL,
    user_id integer NOT NULL,
    movie_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: ratings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ratings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ratings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ratings_id_seq OWNED BY ratings.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: seasons; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE seasons (
    id integer NOT NULL,
    number integer NOT NULL,
    name character varying(50) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    movie_id integer
);


--
-- Name: seasons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE seasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: seasons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE seasons_id_seq OWNED BY seasons.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    nickname character varying(30),
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY actor_fanlists ALTER COLUMN id SET DEFAULT nextval('actor_fanlists_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY actor_histories ALTER COLUMN id SET DEFAULT nextval('actor_histories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY actor_lists ALTER COLUMN id SET DEFAULT nextval('actor_lists_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY actors ALTER COLUMN id SET DEFAULT nextval('actors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY age_ratings ALTER COLUMN id SET DEFAULT nextval('age_ratings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cast_members ALTER COLUMN id SET DEFAULT nextval('cast_members_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY countries ALTER COLUMN id SET DEFAULT nextval('countries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY genres ALTER COLUMN id SET DEFAULT nextval('genres_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_histories ALTER COLUMN id SET DEFAULT nextval('movie_histories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_lists ALTER COLUMN id SET DEFAULT nextval('movie_lists_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_tag_counts ALTER COLUMN id SET DEFAULT nextval('movie_tag_counts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_tags ALTER COLUMN id SET DEFAULT nextval('movie_tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_watchlists ALTER COLUMN id SET DEFAULT nextval('movie_watchlists_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY movies ALTER COLUMN id SET DEFAULT nextval('movies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY ratings ALTER COLUMN id SET DEFAULT nextval('ratings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY seasons ALTER COLUMN id SET DEFAULT nextval('seasons_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: actor_fanlists_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY actor_fanlists
    ADD CONSTRAINT actor_fanlists_pkey PRIMARY KEY (id);


--
-- Name: actor_fanlists_user_id_actor_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY actor_fanlists
    ADD CONSTRAINT actor_fanlists_user_id_actor_id_key UNIQUE (user_id, actor_id);


--
-- Name: actor_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY actor_histories
    ADD CONSTRAINT actor_histories_pkey PRIMARY KEY (id);


--
-- Name: actor_lists_actors_actor_list_id_actor_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY actor_lists_actors
    ADD CONSTRAINT actor_lists_actors_actor_list_id_actor_id_key UNIQUE (actor_list_id, actor_id);


--
-- Name: actor_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY actor_lists
    ADD CONSTRAINT actor_lists_pkey PRIMARY KEY (id);


--
-- Name: actors_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY actors
    ADD CONSTRAINT actors_pkey PRIMARY KEY (id);


--
-- Name: age_ratings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY age_ratings
    ADD CONSTRAINT age_ratings_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: cast_members_movie_id_actor_id_role_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cast_members
    ADD CONSTRAINT cast_members_movie_id_actor_id_role_key UNIQUE (movie_id, actor_id, role);


--
-- Name: cast_members_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cast_members
    ADD CONSTRAINT cast_members_pkey PRIMARY KEY (id);


--
-- Name: countries_movies_movie_id_country_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY countries_movies
    ADD CONSTRAINT countries_movies_movie_id_country_id_key UNIQUE (movie_id, country_id);


--
-- Name: countries_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (id);


--
-- Name: genres_movies_movie_id_genre_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY genres_movies
    ADD CONSTRAINT genres_movies_movie_id_genre_id_key UNIQUE (movie_id, genre_id);


--
-- Name: genres_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY genres
    ADD CONSTRAINT genres_pkey PRIMARY KEY (id);


--
-- Name: movie_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_histories
    ADD CONSTRAINT movie_histories_pkey PRIMARY KEY (id);


--
-- Name: movie_lists_movies_movie_id_movie_list_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_lists_movies
    ADD CONSTRAINT movie_lists_movies_movie_id_movie_list_id_key UNIQUE (movie_id, movie_list_id);


--
-- Name: movie_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_lists
    ADD CONSTRAINT movie_lists_pkey PRIMARY KEY (id);


--
-- Name: movie_tag_counts_movie_id_movie_tag_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_tag_counts
    ADD CONSTRAINT movie_tag_counts_movie_id_movie_tag_id_key UNIQUE (movie_id, movie_tag_id);


--
-- Name: movie_tag_counts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_tag_counts
    ADD CONSTRAINT movie_tag_counts_pkey PRIMARY KEY (id);


--
-- Name: movie_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_tags
    ADD CONSTRAINT movie_tags_pkey PRIMARY KEY (id);


--
-- Name: movie_watchlists_movie_id_user_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_watchlists
    ADD CONSTRAINT movie_watchlists_movie_id_user_id_key UNIQUE (movie_id, user_id);


--
-- Name: movie_watchlists_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_watchlists
    ADD CONSTRAINT movie_watchlists_pkey PRIMARY KEY (id);


--
-- Name: movies_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movies
    ADD CONSTRAINT movies_pkey PRIMARY KEY (id);


--
-- Name: ratings_movie_id_user_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ratings
    ADD CONSTRAINT ratings_movie_id_user_id_key UNIQUE (movie_id, user_id);


--
-- Name: ratings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ratings
    ADD CONSTRAINT ratings_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: seasons_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY seasons
    ADD CONSTRAINT seasons_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_actor_fanlists_on_actor_id_and_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_actor_fanlists_on_actor_id_and_user_id ON actor_fanlists USING btree (actor_id, user_id);


--
-- Name: index_actor_fanlists_on_user_id_and_actor_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_actor_fanlists_on_user_id_and_actor_id ON actor_fanlists USING btree (user_id, actor_id);


--
-- Name: index_actor_histories_on_actor_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_actor_histories_on_actor_id ON actor_histories USING btree (actor_id);


--
-- Name: index_actor_lists_actors_on_actor_id_and_actor_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_actor_lists_actors_on_actor_id_and_actor_list_id ON actor_lists_actors USING btree (actor_id, actor_list_id);


--
-- Name: index_actor_lists_actors_on_actor_list_id_and_actor_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_actor_lists_actors_on_actor_list_id_and_actor_id ON actor_lists_actors USING btree (actor_list_id, actor_id);


--
-- Name: index_age_ratings_on_rating; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_age_ratings_on_rating ON age_ratings USING btree (rating);


--
-- Name: index_cast_members_on_actor_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cast_members_on_actor_id ON cast_members USING btree (actor_id);


--
-- Name: index_cast_members_on_movie_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cast_members_on_movie_id ON cast_members USING btree (movie_id);


--
-- Name: index_cast_members_on_role; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cast_members_on_role ON cast_members USING btree (role);


--
-- Name: index_countries_movies_on_country_id_and_movie_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_countries_movies_on_country_id_and_movie_id ON countries_movies USING btree (country_id, movie_id);


--
-- Name: index_countries_movies_on_movie_id_and_country_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_countries_movies_on_movie_id_and_country_id ON countries_movies USING btree (movie_id, country_id);


--
-- Name: index_genres_movies_on_genre_id_and_movie_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_genres_movies_on_genre_id_and_movie_id ON genres_movies USING btree (genre_id, movie_id);


--
-- Name: index_genres_movies_on_movie_id_and_genre_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_genres_movies_on_movie_id_and_genre_id ON genres_movies USING btree (movie_id, genre_id);


--
-- Name: index_movie_histories_on_movie_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_movie_histories_on_movie_id ON movie_histories USING btree (movie_id);


--
-- Name: index_movie_lists_movies_on_movie_id_and_movie_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_movie_lists_movies_on_movie_id_and_movie_list_id ON movie_lists_movies USING btree (movie_id, movie_list_id);


--
-- Name: index_movie_lists_movies_on_movie_list_id_and_movie_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_movie_lists_movies_on_movie_list_id_and_movie_id ON movie_lists_movies USING btree (movie_list_id, movie_id);


--
-- Name: index_movie_lists_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_movie_lists_on_user_id ON movie_lists USING btree (user_id);


--
-- Name: index_movie_tag_counts_on_movie_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_movie_tag_counts_on_movie_id ON movie_tag_counts USING btree (movie_id);


--
-- Name: index_movie_tag_counts_on_movie_tag_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_movie_tag_counts_on_movie_tag_id ON movie_tag_counts USING btree (movie_tag_id);


--
-- Name: index_movie_watchlists_on_movie_id_and_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_movie_watchlists_on_movie_id_and_user_id ON movie_watchlists USING btree (movie_id, user_id);


--
-- Name: index_movie_watchlists_on_user_id_and_movie_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_movie_watchlists_on_user_id_and_movie_id ON movie_watchlists USING btree (user_id, movie_id);


--
-- Name: index_movies_on_season_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_movies_on_season_id ON movies USING btree (season_id);


--
-- Name: index_movies_on_subdivision; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_movies_on_subdivision ON movies USING btree (subdivision);


--
-- Name: index_ratings_on_movie_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_ratings_on_movie_id ON ratings USING btree (movie_id);


--
-- Name: index_ratings_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_ratings_on_user_id ON ratings USING btree (user_id);


--
-- Name: index_seasons_on_movie_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_seasons_on_movie_id ON seasons USING btree (movie_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: fk_actor_fanlists_actor_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY actor_fanlists
    ADD CONSTRAINT fk_actor_fanlists_actor_id_on_delete_cascade FOREIGN KEY (actor_id) REFERENCES actors(id) ON DELETE CASCADE;


--
-- Name: fk_actor_fanlists_user_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY actor_fanlists
    ADD CONSTRAINT fk_actor_fanlists_user_id_on_delete_cascade FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: fk_actor_histories_actor_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY actor_histories
    ADD CONSTRAINT fk_actor_histories_actor_id_on_delete_cascade FOREIGN KEY (actor_id) REFERENCES actors(id) ON DELETE CASCADE;


--
-- Name: fk_actor_lists_actors_actor_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY actor_lists_actors
    ADD CONSTRAINT fk_actor_lists_actors_actor_id_on_delete_cascade FOREIGN KEY (actor_id) REFERENCES actors(id) ON DELETE CASCADE;


--
-- Name: fk_actor_lists_actors_actor_list_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY actor_lists_actors
    ADD CONSTRAINT fk_actor_lists_actors_actor_list_id_on_delete_cascade FOREIGN KEY (actor_list_id) REFERENCES actor_lists(id) ON DELETE CASCADE;


--
-- Name: fk_actor_lists_user_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY actor_lists
    ADD CONSTRAINT fk_actor_lists_user_id_on_delete_cascade FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: fk_cast_crews_movie_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cast_members
    ADD CONSTRAINT fk_cast_crews_movie_id_on_delete_cascade FOREIGN KEY (movie_id) REFERENCES movies(id) ON DELETE CASCADE;


--
-- Name: fk_cast_members_actor_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cast_members
    ADD CONSTRAINT fk_cast_members_actor_id_on_delete_cascade FOREIGN KEY (actor_id) REFERENCES actors(id) ON DELETE CASCADE;


--
-- Name: fk_countries_movies_country_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY countries_movies
    ADD CONSTRAINT fk_countries_movies_country_id_on_delete_cascade FOREIGN KEY (country_id) REFERENCES countries(id) ON DELETE CASCADE;


--
-- Name: fk_countries_movies_movie_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY countries_movies
    ADD CONSTRAINT fk_countries_movies_movie_id_on_delete_cascade FOREIGN KEY (movie_id) REFERENCES movies(id) ON DELETE CASCADE;


--
-- Name: fk_genres_movies_genre_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY genres_movies
    ADD CONSTRAINT fk_genres_movies_genre_id_on_delete_cascade FOREIGN KEY (genre_id) REFERENCES genres(id) ON DELETE CASCADE;


--
-- Name: fk_genres_movies_movie_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY genres_movies
    ADD CONSTRAINT fk_genres_movies_movie_id_on_delete_cascade FOREIGN KEY (movie_id) REFERENCES movies(id) ON DELETE CASCADE;


--
-- Name: fk_lists_movies_list_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_lists_movies
    ADD CONSTRAINT fk_lists_movies_list_id_on_delete_cascade FOREIGN KEY (movie_list_id) REFERENCES movie_lists(id) ON DELETE CASCADE;


--
-- Name: fk_lists_movies_movie_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_lists_movies
    ADD CONSTRAINT fk_lists_movies_movie_id_on_delete_cascade FOREIGN KEY (movie_id) REFERENCES movies(id) ON DELETE CASCADE;


--
-- Name: fk_lists_user_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_lists
    ADD CONSTRAINT fk_lists_user_id_on_delete_cascade FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: fk_movie_histories_movie_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_histories
    ADD CONSTRAINT fk_movie_histories_movie_id_on_delete_cascade FOREIGN KEY (movie_id) REFERENCES movies(id) ON DELETE CASCADE;


--
-- Name: fk_movie_tag_counts_movie_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_tag_counts
    ADD CONSTRAINT fk_movie_tag_counts_movie_id_on_delete_cascade FOREIGN KEY (movie_id) REFERENCES movies(id) ON DELETE CASCADE;


--
-- Name: fk_movie_tag_counts_movie_tag_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_tag_counts
    ADD CONSTRAINT fk_movie_tag_counts_movie_tag_id_on_delete_cascade FOREIGN KEY (movie_tag_id) REFERENCES movie_tags(id) ON DELETE CASCADE;


--
-- Name: fk_movie_watchlists_movie_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_watchlists
    ADD CONSTRAINT fk_movie_watchlists_movie_id_on_delete_cascade FOREIGN KEY (movie_id) REFERENCES movies(id) ON DELETE CASCADE;


--
-- Name: fk_movie_watchlists_user_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movie_watchlists
    ADD CONSTRAINT fk_movie_watchlists_user_id_on_delete_cascade FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: fk_movies_age_rating_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movies
    ADD CONSTRAINT fk_movies_age_rating_id_on_delete_cascade FOREIGN KEY (age_rating_id) REFERENCES age_ratings(id) ON DELETE CASCADE;


--
-- Name: fk_movies_season_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY movies
    ADD CONSTRAINT fk_movies_season_id_on_delete_cascade FOREIGN KEY (season_id) REFERENCES seasons(id) ON DELETE CASCADE;


--
-- Name: fk_ratings_movie_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ratings
    ADD CONSTRAINT fk_ratings_movie_id_on_delete_cascade FOREIGN KEY (movie_id) REFERENCES movies(id) ON DELETE CASCADE;


--
-- Name: fk_ratings_user_id_on_delete_restrict; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ratings
    ADD CONSTRAINT fk_ratings_user_id_on_delete_restrict FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE RESTRICT;


--
-- Name: fk_seasons_movie_id_on_delete_cascade; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY seasons
    ADD CONSTRAINT fk_seasons_movie_id_on_delete_cascade FOREIGN KEY (movie_id) REFERENCES movies(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO schema_migrations (version) VALUES
('20170114125607'),
('20170118002019'),
('20170118010422'),
('20170118021122'),
('20170118054513'),
('20170118151911'),
('20170118152530'),
('20170118161214'),
('20170118162939'),
('20170118181945'),
('20170118182644'),
('20170119122841'),
('20170119122952'),
('20170119143742'),
('20170119150125'),
('20170120003057'),
('20170120003552'),
('20170120005730'),
('20170120010809'),
('20170120011200'),
('20170120125516'),
('20170120141214'),
('20170121192654'),
('20170122002200'),
('20170123150702'),
('20170123160714'),
('20170123162544'),
('20170123175555'),
('20170123215927'),
('20170124175706'),
('20170124210237'),
('20170127032658'),
('20170127064652'),
('20170127075308'),
('20170127075838'),
('20170127080914'),
('20170127080958'),
('20170129143910'),
('20170129152747'),
('20170129154537'),
('20170129155524'),
('20170129164325'),
('20170129164855'),
('20170129185225'),
('20170129233857'),
('20170129234016'),
('20170129234520'),
('20170129235431'),
('20170130121009'),
('20170130122117'),
('20170130134408'),
('20170130134711'),
('20170130135119'),
('20170130141421'),
('20170130142650'),
('20170130203507');


