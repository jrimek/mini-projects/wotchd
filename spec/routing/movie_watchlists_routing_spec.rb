require "rails_helper"

RSpec.describe MovieWatchlistsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/movie_watchlists").to route_to("movie_watchlists#index")
    end

    it "routes to #new" do
      expect(:get => "/movie_watchlists/new").to route_to("movie_watchlists#new")
    end

    it "routes to #show" do
      expect(:get => "/movie_watchlists/1").to route_to("movie_watchlists#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/movie_watchlists/1/edit").to route_to("movie_watchlists#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/movie_watchlists").to route_to("movie_watchlists#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/movie_watchlists/1").to route_to("movie_watchlists#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/movie_watchlists/1").to route_to("movie_watchlists#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/movie_watchlists/1").to route_to("movie_watchlists#destroy", :id => "1")
    end

  end
end
