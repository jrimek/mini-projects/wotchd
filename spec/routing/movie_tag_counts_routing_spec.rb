require "rails_helper"

RSpec.describe MovieTagCountsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/movie_tag_counts").to route_to("movie_tag_counts#index")
    end

    it "routes to #new" do
      expect(:get => "/movie_tag_counts/new").to route_to("movie_tag_counts#new")
    end

    it "routes to #show" do
      expect(:get => "/movie_tag_counts/1").to route_to("movie_tag_counts#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/movie_tag_counts/1/edit").to route_to("movie_tag_counts#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/movie_tag_counts").to route_to("movie_tag_counts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/movie_tag_counts/1").to route_to("movie_tag_counts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/movie_tag_counts/1").to route_to("movie_tag_counts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/movie_tag_counts/1").to route_to("movie_tag_counts#destroy", :id => "1")
    end

  end
end
