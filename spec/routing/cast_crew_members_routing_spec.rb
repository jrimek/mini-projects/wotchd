require "rails_helper"

RSpec.describe CastCrewMembersController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cast_crew_members").to route_to("cast_crew_members#index")
    end

    it "routes to #new" do
      expect(:get => "/cast_crew_members/new").to route_to("cast_crew_members#new")
    end

    it "routes to #show" do
      expect(:get => "/cast_crew_members/1").to route_to("cast_crew_members#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cast_crew_members/1/edit").to route_to("cast_crew_members#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cast_crew_members").to route_to("cast_crew_members#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/cast_crew_members/1").to route_to("cast_crew_members#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/cast_crew_members/1").to route_to("cast_crew_members#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cast_crew_members/1").to route_to("cast_crew_members#destroy", :id => "1")
    end

  end
end
