require "rails_helper"

RSpec.describe AgeRatingsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/age_ratings").to route_to("age_ratings#index")
    end

    it "routes to #new" do
      expect(:get => "/age_ratings/new").to route_to("age_ratings#new")
    end

    it "routes to #show" do
      expect(:get => "/age_ratings/1").to route_to("age_ratings#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/age_ratings/1/edit").to route_to("age_ratings#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/age_ratings").to route_to("age_ratings#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/age_ratings/1").to route_to("age_ratings#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/age_ratings/1").to route_to("age_ratings#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/age_ratings/1").to route_to("age_ratings#destroy", :id => "1")
    end

  end
end
