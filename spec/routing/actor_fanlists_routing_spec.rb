require "rails_helper"

RSpec.describe ActorFanlistsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/actor_fanlists").to route_to("actor_fanlists#index")
    end

    it "routes to #new" do
      expect(:get => "/actor_fanlists/new").to route_to("actor_fanlists#new")
    end

    it "routes to #show" do
      expect(:get => "/actor_fanlists/1").to route_to("actor_fanlists#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/actor_fanlists/1/edit").to route_to("actor_fanlists#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/actor_fanlists").to route_to("actor_fanlists#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/actor_fanlists/1").to route_to("actor_fanlists#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/actor_fanlists/1").to route_to("actor_fanlists#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/actor_fanlists/1").to route_to("actor_fanlists#destroy", :id => "1")
    end

  end
end
