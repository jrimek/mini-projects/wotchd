require "rails_helper"

RSpec.describe MovieTagsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/movie_tags").to route_to("movie_tags#index")
    end

    it "routes to #new" do
      expect(:get => "/movie_tags/new").to route_to("movie_tags#new")
    end

    it "routes to #show" do
      expect(:get => "/movie_tags/1").to route_to("movie_tags#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/movie_tags/1/edit").to route_to("movie_tags#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/movie_tags").to route_to("movie_tags#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/movie_tags/1").to route_to("movie_tags#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/movie_tags/1").to route_to("movie_tags#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/movie_tags/1").to route_to("movie_tags#destroy", :id => "1")
    end

  end
end
