require "rails_helper"

RSpec.describe MovieHistoriesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/movie_histories").to route_to("movie_histories#index")
    end

    it "routes to #new" do
      expect(:get => "/movie_histories/new").to route_to("movie_histories#new")
    end

    it "routes to #show" do
      expect(:get => "/movie_histories/1").to route_to("movie_histories#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/movie_histories/1/edit").to route_to("movie_histories#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/movie_histories").to route_to("movie_histories#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/movie_histories/1").to route_to("movie_histories#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/movie_histories/1").to route_to("movie_histories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/movie_histories/1").to route_to("movie_histories#destroy", :id => "1")
    end

  end
end
