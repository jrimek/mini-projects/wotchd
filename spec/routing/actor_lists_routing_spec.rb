require "rails_helper"

RSpec.describe ActorListsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/actor_lists").to route_to("actor_lists#index")
    end

    it "routes to #new" do
      expect(:get => "/actor_lists/new").to route_to("actor_lists#new")
    end

    it "routes to #show" do
      expect(:get => "/actor_lists/1").to route_to("actor_lists#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/actor_lists/1/edit").to route_to("actor_lists#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/actor_lists").to route_to("actor_lists#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/actor_lists/1").to route_to("actor_lists#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/actor_lists/1").to route_to("actor_lists#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/actor_lists/1").to route_to("actor_lists#destroy", :id => "1")
    end

  end
end
