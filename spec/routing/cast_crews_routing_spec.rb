require "rails_helper"

RSpec.describe CastCrewsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cast_crews").to route_to("cast_crews#index")
    end

    it "routes to #new" do
      expect(:get => "/cast_crews/new").to route_to("cast_crews#new")
    end

    it "routes to #show" do
      expect(:get => "/cast_crews/1").to route_to("cast_crews#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cast_crews/1/edit").to route_to("cast_crews#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cast_crews").to route_to("cast_crews#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/cast_crews/1").to route_to("cast_crews#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/cast_crews/1").to route_to("cast_crews#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cast_crews/1").to route_to("cast_crews#destroy", :id => "1")
    end

  end
end
