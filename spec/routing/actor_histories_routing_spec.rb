require "rails_helper"

RSpec.describe ActorHistoriesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/actor_histories").to route_to("actor_histories#index")
    end

    it "routes to #new" do
      expect(:get => "/actor_histories/new").to route_to("actor_histories#new")
    end

    it "routes to #show" do
      expect(:get => "/actor_histories/1").to route_to("actor_histories#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/actor_histories/1/edit").to route_to("actor_histories#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/actor_histories").to route_to("actor_histories#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/actor_histories/1").to route_to("actor_histories#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/actor_histories/1").to route_to("actor_histories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/actor_histories/1").to route_to("actor_histories#destroy", :id => "1")
    end

  end
end
