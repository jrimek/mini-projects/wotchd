require 'rails_helper'

RSpec.describe MovieTag, type: :model do
  it "must have a name" do
    movie_tag = build :movie_tag, name: 'beschte'
    expect(movie_tag).to be_valid
  end

  it "can't have no name" do
    no_name = build :movie_tag, name: nil
    expect(no_name).to be_invalid
  end

  it "can't have a name with with +50 characters" do
    too_long = build :movie_tag, name: 'too_long name allalalalalalalalallaallalalalalalala'
    expect(too_long).to be_invalid
  end
end
