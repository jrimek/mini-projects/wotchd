require 'rails_helper'

RSpec.describe MovieWatchlist, type: :model do
  user = FactoryGirl.create :user, email: 'movie_watchlist@test.de'
  age_rating = FactoryGirl.create :age_rating
  movie = FactoryGirl.create :movie, age_rating_id: age_rating.id

  it "must have an movie_id and a user_id" do
    watchlist = build :movie_watchlist, movie_id: movie.id, user_id: user.id
    expect(watchlist).to be_valid
  end

  it "can't have no movie_id" do
    watchlist = build :movie_watchlist, movie_id: nil, user_id: user.id
    expect(watchlist).to be_invalid
  end

  it "can't have no user_id" do
    watchlist = build :movie_watchlist, movie_id: movie.id, user_id: nil
    expect(watchlist).to be_invalid
  end

  it "can't have duplicate movies on a watchlist" do
    watchlist = create :movie_watchlist, movie_id: movie.id, user_id: user.id
    expect(watchlist).to be_valid

    duplicate_watchlist = build :movie_watchlist, movie_id: movie.id, user_id: user.id
    expect(duplicate_watchlist).to be_invalid
  end

end
