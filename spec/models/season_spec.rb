require 'rails_helper'

RSpec.describe Season, type: :model do
  age_rating = FactoryGirl.create :age_rating
  movie = FactoryGirl.create :movie, age_rating_id: age_rating.id

  it "must have a name and a number and a movie_id" do
    season = build :season, number: 1, name: 'GOT SEASON 1'#, movie_id: 1
    expect(season).to be_valid
  end

  it "can't have no name" do
    no_name = build :season, number: 1, name: nil
    expect(no_name).to be_invalid
  end

  it "can't have no number" do
    no_number = build :season, number: nil, name: 'GOT SEASON 1'
    expect(no_number).to be_invalid
  end

  it "can't have a name with with +50 characters" do
    too_long = build :season, number: 1, name: 'too_long name allalalalalalalalallaallalalalalalala'
    expect(too_long).to be_invalid
  end
end
