require 'rails_helper'

RSpec.describe ActorFanlist, type: :model do
  user = FactoryGirl.create :user, email: 'actor_fanlist@test.de'
  actor = FactoryGirl.create :actor

  it "must have an actor_id and a user_id" do
    fanlist = build :actor_fanlist, actor_id: actor.id, user_id: user.id
    expect(fanlist).to be_valid
  end

  it "can't have no actor_id" do
    fanlist = build :actor_fanlist, actor_id: nil, user_id: user.id
    expect(fanlist).to be_invalid
  end

  it "can't have no user_id" do
    fanlist = build :actor_fanlist, actor_id: actor.id, user_id: nil
    expect(fanlist).to be_invalid
  end

  it "can't have duplicate actors on a fanlist" do
    fanlist = create :actor_fanlist, actor_id: actor.id, user_id: user.id
    expect(fanlist).to be_valid

    duplicate_fanlist = build :actor_fanlist, actor_id: actor.id, user_id: user.id
    expect(duplicate_fanlist).to be_invalid
  end


end
