require 'rails_helper'

RSpec.describe CastMember, type: :model do
  age_rating = FactoryGirl.create :age_rating
  movie = FactoryGirl.create :movie, age_rating_id: age_rating.id
  actor = FactoryGirl.create :actor

  it "must have a movie_id, actor_id and a role" do
    cast = build :cast_member, movie_id: movie.id,
                             actor_id: actor.id,
                             role: CastMember.roles[:actor]
    expect(cast).to be_valid
  end

  it "can't have no movie" do
    no_movie = build :cast_member, movie_id: nil,
                                 actor_id: actor.id,
                                 role: CastMember.roles[:actor]
    expect(no_movie).to be_invalid
  end

  it "can't have no actor" do
    no_actor = build :cast_member, movie_id: 1676848,
                                       actor_id: nil,
                                       role: CastMember.roles[:actor]
    expect(no_actor).to be_invalid
  end

  it "can't have no role" do
    no_role = build :cast_member, movie_id: 1676848,
                                          actor_id: actor.id,
                                          role: nil
    expect(no_role).to be_invalid
  end
end
