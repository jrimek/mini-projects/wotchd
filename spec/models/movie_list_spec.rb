require 'rails_helper'

RSpec.describe MovieList, type: :model do
  user = FactoryGirl.create :user, email: 'movie_list@test.de'
  #factory :movie_list do
  #  name 'user_watchlist'
  #  watchlist true
  #  user_id nil
  #end

  it "must have user_id, name" do
    list = build :movie_list, user_id: user.id
    expect(list).to be_valid
  end

  it "can't have no user" do
    no_user = build :movie_list, user_id: nil
    expect(no_user).to be_invalid
  end

  it "can't have no name" do
    no_name = build :movie_list, name: nil, user_id: user.id
    expect(no_name).to be_invalid
  end

  it "can't have a name with with +50 characters" do
    too_long = build :movie_list, user_id: user.id, name: 'too_long name allalalalalalalalallaallalalalalalala'
    expect(too_long).to be_invalid
  end

  it "can't have the same movie twice" do
    age_rating = FactoryGirl.create :age_rating
    movie = FactoryGirl.create :movie, age_rating_id: age_rating.id
    list = create :movie_list, user_id: user.id

    list.add_movie movie
    count = list.movies.size

    list.add_movie movie
    expect(list.movies.size).to equal(count)
  end
end
