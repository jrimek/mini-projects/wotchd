require 'rails_helper'

RSpec.describe User, type: :model do
  it "must have an email and a password" do
    user = build :user, email: "test@test.de", password: "test123"
    expect(user).to be_valid
  end

  it "can't have no email" do
    no_email = build :user, email: nil, password: 'test123'
    expect(no_email).to be_invalid
  end

  it "can't have no password" do
    no_pw = build :user, email: "test@test.de", password: nil
    expect(no_pw).to be_invalid
  end

  it "can't have a name with with +30 characters" do
    too_long = build :user, nickname: 'too_long name allalalalalalalal'
    expect(too_long).to be_invalid
  end
end
