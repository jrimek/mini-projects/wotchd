require 'rails_helper'

RSpec.describe Rating, type: :model do
  #factory :rating do
  #  title nil
  #  review nil
  #  rating "9.5"
  #  user nil
  #  movie nil
  #end

  user = FactoryGirl.create :user, email: 'rating@test.de'
  age_rating = FactoryGirl.create :age_rating
  movie = FactoryGirl.create :movie, age_rating_id: age_rating.id

  it "must have a rating, movie_id and user id" do
    rating = build :rating, movie_id: movie.id, user_id: user.id
    expect(rating).to be_valid
  end

  it "can't have no rating" do
    no_rating = build :rating, rating: nil, movie_id: movie.id, user_id: user.id
    expect(no_rating).to be_invalid
  end

  it "can't have no movie" do
    no_movie = build :rating, user_id: user.id
    expect(no_movie).to be_invalid
  end

  it "can't have no user" do
    no_user = build :rating, movie_id: movie.id
    expect(no_user).to be_invalid
  end

end
