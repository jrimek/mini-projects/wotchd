require 'rails_helper'

RSpec.describe Actor, type: :model do
  #factory :actor do
  #  first_name  'Hayley'
  #  last_name  'McFarland'
  #  sex   'Frau'
  #end

  it "must have a name and a sex" do
    cast_member = build :actor
    expect(cast_member).to be_valid
  end

  it "can't have no first_name" do
    no_name = build :actor, first_name: nil
    expect(no_name).to be_invalid
  end

  it "can't have no sex" do
    no_sex = build :actor, sex: nil
    expect(no_sex).to be_invalid
  end

  it "can't have a first name with with +50 characters" do
    too_long = build :actor, first_name: 'too_long name allalalalalalalalallaallalalalalalala'
    expect(too_long).to be_invalid
  end

  it "can't have a middle name with with +50 characters" do
    too_long = build :actor, middle_name: 'too_long name allalalalalalalalallaallalalalalalala'
    expect(too_long).to be_invalid
  end

  it "can't have a last name with with +50 characters" do
    too_long = build :actor, last_name: 'too_long name allalalalalalalalallaallalalalalalala'
    expect(too_long).to be_invalid
  end

end
