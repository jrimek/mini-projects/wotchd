require 'rails_helper'

RSpec.describe Country, type: :model do
  it "must have a name" do
    country = build :country, name: 'DE'
    expect(country).to be_valid
  end

  it "can't have no name" do
    no_name = build :country, name: nil
    expect(no_name).to be_invalid
  end
end
