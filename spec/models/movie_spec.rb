require 'rails_helper'

RSpec.describe Movie, type: :model do
  # factory :movie do
  #   engl_title 'District 9'
  #   duration 150
  #   subdivision 'Movie'
  #   release_date Date.today
  #   age_rating nil
  # end
  age_rating = FactoryGirl.create :age_rating

  it "must have an english title, duration, subdivision and a release date" do
    movie = build :movie, age_rating_id: age_rating.id
    expect(movie).to be_valid
  end

  it "can't have no engl_title" do
    no_engl_title = build :movie, engl_title: nil, age_rating_id: age_rating.id
    expect(no_engl_title).to be_invalid
  end

  it "can't have no duration" do
    no_duration = build :movie, duration: nil, age_rating_id: age_rating.id
    expect(no_duration).to be_invalid
  end

  it "can't have no subdivision" do
    no_subdivision = build :movie, subdivision: nil, age_rating_id: age_rating.id
    expect(no_subdivision).to be_invalid
  end

  it "can't have no release_date" do
    no_release_date = build :movie, release_date: nil, age_rating_id: age_rating.id
    expect(no_release_date).to be_invalid
  end

  it "can't have an engl title with with +100 characters" do
    too_long = build :movie, age_rating_id: age_rating.id, engl_title: 'too_long name allalalalalalalalallaallalalalalalalatoo_long name allalalalalalalalallaallalalalalalal'
    expect(too_long).to be_invalid
  end

  it "can't have an ger title with with +100 characters" do
    too_long = build :movie, age_rating_id: age_rating.id, ger_title: 'too_long name allalalalalalalalallaallalalalalalalatoo_long name allalalalalalalalallaallalalalalalal'
    expect(too_long).to be_invalid
  end

  it "creates a new MovieTagCount or increments count in the existing one" do
    movie_tag = create :movie_tag
    movie = create :movie, age_rating_id: age_rating.id

    movie.movie_tags << movie_tag
    expect(movie.movie_tags.size).to eq(1)
    expect(movie.movie_tag_counts.first.count).to eq(1)

    movie.movie_tags << movie_tag
    expect(movie.movie_tags.size).to eq(1)
    expect(movie.movie_tag_counts.first.count).to eq(2)
  end

  it "checks if a cast crew member with this role is already part of the movie, if not it adds him" do
    movie = create :movie, age_rating_id: age_rating.id
    actor = create :actor

    movie.add_actor_with_role(actor, 'actor')
    expect(movie.cast_members.size).to eq(1)

    movie.add_actor_with_role(actor, 'actor')
    expect(movie.cast_members.size).to eq(1)

    movie.add_actor_with_role(actor, 'screenwriter')
    expect(movie.cast_members.size).to eq(2)
  end
end
