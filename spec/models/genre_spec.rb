require 'rails_helper'

RSpec.describe Genre, type: :model do
  it "must have a name" do
    genre = build :genre, name: 'drama'
    expect(genre).to be_valid
  end

  it "can't have no name" do
    no_name = build :genre, name: nil
    expect(no_name).to be_invalid
  end

  it "can't have a name with with +50 characters" do
    too_long = build :genre, name: 'too_long name allalalalalalalalallaallalalalalalala'
    expect(too_long).to be_invalid
  end
end
