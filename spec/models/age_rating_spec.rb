require 'rails_helper'

RSpec.describe AgeRating, type: :model do
  it "must have a rating" do
    age_rating = build :age_rating, rating: 'FSK 0'
    expect(age_rating).to be_valid

    no_region = build :age_rating, rating: nil
    expect(no_region).to be_invalid
  end
end
