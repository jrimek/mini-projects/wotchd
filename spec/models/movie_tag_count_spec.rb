require 'rails_helper'

RSpec.describe MovieTagCount, type: :model do
  age_rating = FactoryGirl.create :age_rating
  movie = FactoryGirl.create :movie, age_rating_id: age_rating.id
  movie_tag = FactoryGirl.create :movie_tag

  it "must have a movie_id, movie_tag_id and count" do
    movie_tag_count = build :movie_tag_count, movie_id: movie.id,
                                              movie_tag_id: movie_tag.id,
                                              count: 1
    expect(movie_tag_count).to be_valid
  end

  it "must have no movie" do
    no_movie = build :movie_tag_count, movie_id: nil,
                                       movie_tag_id: movie_tag.id,
                                       count: 1
    expect(no_movie).to be_invalid
  end

  it "must have no movie_tag" do
    no_movie_tag = build :movie_tag_count, movie_id: movie.id,
                                       movie_tag_id: nil,
                                       count: 1
    expect(no_movie_tag).to be_invalid
  end

  it "must have no count" do
    no_count = build :movie_tag_count, movie_id: movie.id,
                                       movie_tag_id: movie_tag.id,
                                       count: nil
    expect(no_count).to be_invalid
  end
end
