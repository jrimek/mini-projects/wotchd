FactoryGirl.define do
  factory :country do
    name "DE"
  end

  factory :age_rating do
    rating 'FSK 0'
  end

  factory :actor do
    first_name  'Hayley'
    last_name  'McFarland'
    sex   'Frau'
  end

  factory :genre do
    name 'drama'
  end

  factory :movie do
    engl_title 'District 9'
    duration 150
    subdivision 'Movie'
    release_date Date.today
    age_rating nil
  end

  factory :movie_tag do
    name 'beschte'
  end

  factory :user do
    email 'test@test.de'
    password 'test123'
  end

  factory :movie_list do
    name 'user_watchlist'
    user_id nil
  end

  factory :list_element do
    list_id nil
    movie_id nil
  end

  factory :cast_member do
    movie_id nil
    actor_id nil
    role CastMember.roles[:actor]
  end

  factory :movie_tag_count do
    movie_id nil
    movie_tag_id nil
    count 1
  end

  factory :season do
    number 1
    name 'GOT SEASON 1'
    #movie_id
  end
end
