FactoryGirl.define do
  factory :rating do
    title nil
    review nil
    rating "9.5"
    user nil
    movie nil
  end
end
