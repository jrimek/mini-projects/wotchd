require 'rails_helper'

RSpec.describe "actor_lists/show", type: :view do
  before(:each) do
    @actor_list = assign(:actor_list, ActorList.create!(
      :name => "Name",
      :fan_list => false,
      :user_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/2/)
  end
end
