require 'rails_helper'

RSpec.describe "actor_lists/index", type: :view do
  before(:each) do
    assign(:actor_lists, [
      ActorList.create!(
        :name => "Name",
        :fan_list => false,
        :user_id => 2
      ),
      ActorList.create!(
        :name => "Name",
        :fan_list => false,
        :user_id => 2
      )
    ])
  end

  it "renders a list of actor_lists" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
