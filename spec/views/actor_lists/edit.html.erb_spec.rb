require 'rails_helper'

RSpec.describe "actor_lists/edit", type: :view do
  before(:each) do
    @actor_list = assign(:actor_list, ActorList.create!(
      :name => "MyString",
      :fan_list => false,
      :user_id => 1
    ))
  end

  it "renders the edit actor_list form" do
    render

    assert_select "form[action=?][method=?]", actor_list_path(@actor_list), "post" do

      assert_select "input#actor_list_name[name=?]", "actor_list[name]"

      assert_select "input#actor_list_fan_list[name=?]", "actor_list[fan_list]"

      assert_select "input#actor_list_user_id[name=?]", "actor_list[user_id]"
    end
  end
end
