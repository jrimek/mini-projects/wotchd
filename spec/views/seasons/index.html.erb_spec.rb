require 'rails_helper'

RSpec.describe "seasons/index", type: :view do

  before { skip("TODO") }

  before(:each) do
    assign(:seasons, [
      Season.create!(
        :number => 2,
        :name => "Name"
      ),
      Season.create!(
        :number => 2,
        :name => "Name"
      )
    ])
  end

  it "renders a list of seasons" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
