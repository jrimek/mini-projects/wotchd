require 'rails_helper'

RSpec.describe "seasons/edit", type: :view do

  before { skip("TODO") }

  before(:each) do
    @season = assign(:season, Season.create!(
      :number => 1,
      :name => "MyString"
    ))
  end

  it "renders the edit season form" do
    render

    assert_select "form[action=?][method=?]", season_path(@season), "post" do

      assert_select "input#season_number[name=?]", "season[number]"

      assert_select "input#season_name[name=?]", "season[name]"
    end
  end
end
