require 'rails_helper'

RSpec.describe "seasons/show", type: :view do

  before { skip("TODO") }

  before(:each) do
    @season = assign(:season, Season.create!(
      :number => 2,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Name/)
  end
end
