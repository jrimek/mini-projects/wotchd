require 'rails_helper'

RSpec.describe "seasons/new", type: :view do

  before { skip("TODO") }

  before(:each) do
    assign(:season, Season.new(
      :number => 1,
      :name => "MyString"
    ))
  end

  it "renders new season form" do
    render

    assert_select "form[action=?][method=?]", seasons_path, "post" do

      assert_select "input#season_number[name=?]", "season[number]"

      assert_select "input#season_name[name=?]", "season[name]"
    end
  end
end
