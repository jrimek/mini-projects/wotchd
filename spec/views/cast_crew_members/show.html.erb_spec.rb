require 'rails_helper'

RSpec.describe "cast_crew_members/show", type: :view do

  before { skip("TODO") }

  before(:each) do
    @cast_crew_member = assign(:cast_crew_member, CastCrewMember.create!(
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
  end
end
