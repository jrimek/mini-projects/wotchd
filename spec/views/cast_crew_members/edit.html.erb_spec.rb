require 'rails_helper'

RSpec.describe "cast_crew_members/edit", type: :view do

  before { skip("TODO") }

  before(:each) do
    @cast_crew_member = assign(:cast_crew_member, CastCrewMember.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit cast_crew_member form" do
    render

    assert_select "form[action=?][method=?]", cast_crew_member_path(@cast_crew_member), "post" do

      assert_select "input#cast_crew_member_name[name=?]", "cast_crew_member[name]"
    end
  end
end
