require 'rails_helper'

RSpec.describe "cast_crew_members/index", type: :view do

  before { skip("TODO") }

  before(:each) do
    assign(:cast_crew_members, [
      CastCrewMember.create!(
        :name => "Name"
      ),
      CastCrewMember.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of cast_crew_members" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
