require 'rails_helper'

RSpec.describe "cast_crew_members/new", type: :view do

  before { skip("TODO") }

  before(:each) do
    assign(:cast_crew_member, CastCrewMember.new(
      :name => "MyString"
    ))
  end

  it "renders new cast_crew_member form" do
    render

    assert_select "form[action=?][method=?]", cast_crew_members_path, "post" do

      assert_select "input#cast_crew_member_name[name=?]", "cast_crew_member[name]"
    end
  end
end
