require 'rails_helper'

RSpec.describe "movie_watchlists/edit", type: :view do
  before(:each) do
    @movie_watchlist = assign(:movie_watchlist, MovieWatchlist.create!(
      :movie_id => 1,
      :user_id => 1
    ))
  end

  it "renders the edit movie_watchlist form" do
    render

    assert_select "form[action=?][method=?]", movie_watchlist_path(@movie_watchlist), "post" do

      assert_select "input#movie_watchlist_movie_id[name=?]", "movie_watchlist[movie_id]"

      assert_select "input#movie_watchlist_user_id[name=?]", "movie_watchlist[user_id]"
    end
  end
end
