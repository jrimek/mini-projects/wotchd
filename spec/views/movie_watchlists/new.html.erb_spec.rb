require 'rails_helper'

RSpec.describe "movie_watchlists/new", type: :view do
  before(:each) do
    assign(:movie_watchlist, MovieWatchlist.new(
      :movie_id => 1,
      :user_id => 1
    ))
  end

  it "renders new movie_watchlist form" do
    render

    assert_select "form[action=?][method=?]", movie_watchlists_path, "post" do

      assert_select "input#movie_watchlist_movie_id[name=?]", "movie_watchlist[movie_id]"

      assert_select "input#movie_watchlist_user_id[name=?]", "movie_watchlist[user_id]"
    end
  end
end
