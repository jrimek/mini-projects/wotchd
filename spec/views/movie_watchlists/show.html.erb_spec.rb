require 'rails_helper'

RSpec.describe "movie_watchlists/show", type: :view do
  before(:each) do
    @movie_watchlist = assign(:movie_watchlist, MovieWatchlist.create!(
      :movie_id => 2,
      :user_id => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
  end
end
