require 'rails_helper'

RSpec.describe "movie_watchlists/index", type: :view do
  before(:each) do
    assign(:movie_watchlists, [
      MovieWatchlist.create!(
        :movie_id => 2,
        :user_id => 3
      ),
      MovieWatchlist.create!(
        :movie_id => 2,
        :user_id => 3
      )
    ])
  end

  it "renders a list of movie_watchlists" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
