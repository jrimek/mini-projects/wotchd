require 'rails_helper'

RSpec.describe "cast_crews/new", type: :view do

  before { skip("TODO") }

  before(:each) do
    assign(:cast_crew, CastCrew.new(
      :movie => nil,
      :cast_crew_member => nil
    ))
  end

  it "renders new cast_crew form" do
    render

    assert_select "form[action=?][method=?]", cast_crews_path, "post" do

      assert_select "input#cast_crew_movie_id[name=?]", "cast_crew[movie_id]"

      assert_select "input#cast_crew_cast_crew_member_id[name=?]", "cast_crew[cast_crew_member_id]"
    end
  end
end
