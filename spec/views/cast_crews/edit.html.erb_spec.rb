require 'rails_helper'

RSpec.describe "cast_crews/edit", type: :view do

  before { skip("TODO") }

  before(:each) do
    @cast_crew = assign(:cast_crew, CastCrew.create!(
      :movie => nil,
      :cast_crew_member => nil
    ))
  end

  it "renders the edit cast_crew form" do
    render

    assert_select "form[action=?][method=?]", cast_crew_path(@cast_crew), "post" do

      assert_select "input#cast_crew_movie_id[name=?]", "cast_crew[movie_id]"

      assert_select "input#cast_crew_cast_crew_member_id[name=?]", "cast_crew[cast_crew_member_id]"
    end
  end
end
