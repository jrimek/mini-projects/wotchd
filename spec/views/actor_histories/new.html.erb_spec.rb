require 'rails_helper'

RSpec.describe "actor_histories/new", type: :view do
  before(:each) do
    assign(:actor_history, ActorHistory.new(
      :actor_id => 1,
      :actor_content => ""
    ))
  end

  it "renders new actor_history form" do
    render

    assert_select "form[action=?][method=?]", actor_histories_path, "post" do

      assert_select "input#actor_history_actor_id[name=?]", "actor_history[actor_id]"

      assert_select "input#actor_history_actor_content[name=?]", "actor_history[actor_content]"
    end
  end
end
