require 'rails_helper'

RSpec.describe "actor_histories/show", type: :view do
  before(:each) do
    @actor_history = assign(:actor_history, ActorHistory.create!(
      :actor_id => 2,
      :actor_content => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(//)
  end
end
