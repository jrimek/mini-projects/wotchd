require 'rails_helper'

RSpec.describe "actor_histories/index", type: :view do
  before(:each) do
    assign(:actor_histories, [
      ActorHistory.create!(
        :actor_id => 2,
        :actor_content => ""
      ),
      ActorHistory.create!(
        :actor_id => 2,
        :actor_content => ""
      )
    ])
  end

  it "renders a list of actor_histories" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
