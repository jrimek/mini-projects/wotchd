require 'rails_helper'

RSpec.describe "actor_histories/edit", type: :view do
  before(:each) do
    @actor_history = assign(:actor_history, ActorHistory.create!(
      :actor_id => 1,
      :actor_content => ""
    ))
  end

  it "renders the edit actor_history form" do
    render

    assert_select "form[action=?][method=?]", actor_history_path(@actor_history), "post" do

      assert_select "input#actor_history_actor_id[name=?]", "actor_history[actor_id]"

      assert_select "input#actor_history_actor_content[name=?]", "actor_history[actor_content]"
    end
  end
end
