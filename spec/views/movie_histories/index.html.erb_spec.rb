require 'rails_helper'

RSpec.describe "movie_histories/index", type: :view do
  before(:each) do
    assign(:movie_histories, [
      MovieHistory.create!(
        :movie_id => "",
        :movie_content => ""
      ),
      MovieHistory.create!(
        :movie_id => "",
        :movie_content => ""
      )
    ])
  end

  it "renders a list of movie_histories" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
