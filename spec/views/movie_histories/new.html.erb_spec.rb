require 'rails_helper'

RSpec.describe "movie_histories/new", type: :view do
  before(:each) do
    assign(:movie_history, MovieHistory.new(
      :movie_id => "",
      :movie_content => ""
    ))
  end

  it "renders new movie_history form" do
    render

    assert_select "form[action=?][method=?]", movie_histories_path, "post" do

      assert_select "input#movie_history_movie_id[name=?]", "movie_history[movie_id]"

      assert_select "input#movie_history_movie_content[name=?]", "movie_history[movie_content]"
    end
  end
end
