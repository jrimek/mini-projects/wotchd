require 'rails_helper'

RSpec.describe "movie_histories/edit", type: :view do
  before(:each) do
    @movie_history = assign(:movie_history, MovieHistory.create!(
      :movie_id => "",
      :movie_content => ""
    ))
  end

  it "renders the edit movie_history form" do
    render

    assert_select "form[action=?][method=?]", movie_history_path(@movie_history), "post" do

      assert_select "input#movie_history_movie_id[name=?]", "movie_history[movie_id]"

      assert_select "input#movie_history_movie_content[name=?]", "movie_history[movie_content]"
    end
  end
end
