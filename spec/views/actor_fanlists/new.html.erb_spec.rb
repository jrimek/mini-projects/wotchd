require 'rails_helper'

RSpec.describe "actor_fanlists/new", type: :view do
  before(:each) do
    assign(:actor_fanlist, ActorFanlist.new(
      :actor_id => 1,
      :user_id => 1
    ))
  end

  it "renders new actor_fanlist form" do
    render

    assert_select "form[action=?][method=?]", actor_fanlists_path, "post" do

      assert_select "input#actor_fanlist_actor_id[name=?]", "actor_fanlist[actor_id]"

      assert_select "input#actor_fanlist_user_id[name=?]", "actor_fanlist[user_id]"
    end
  end
end
