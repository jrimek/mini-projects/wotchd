require 'rails_helper'

RSpec.describe "actor_fanlists/edit", type: :view do
  before(:each) do
    @actor_fanlist = assign(:actor_fanlist, ActorFanlist.create!(
      :actor_id => 1,
      :user_id => 1
    ))
  end

  it "renders the edit actor_fanlist form" do
    render

    assert_select "form[action=?][method=?]", actor_fanlist_path(@actor_fanlist), "post" do

      assert_select "input#actor_fanlist_actor_id[name=?]", "actor_fanlist[actor_id]"

      assert_select "input#actor_fanlist_user_id[name=?]", "actor_fanlist[user_id]"
    end
  end
end
