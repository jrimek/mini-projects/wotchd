require 'rails_helper'

RSpec.describe "actor_fanlists/show", type: :view do
  before(:each) do
    @actor_fanlist = assign(:actor_fanlist, ActorFanlist.create!(
      :actor_id => 2,
      :user_id => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
  end
end
