require 'rails_helper'

RSpec.describe "actor_fanlists/index", type: :view do
  before(:each) do
    assign(:actor_fanlists, [
      ActorFanlist.create!(
        :actor_id => 2,
        :user_id => 3
      ),
      ActorFanlist.create!(
        :actor_id => 2,
        :user_id => 3
      )
    ])
  end

  it "renders a list of actor_fanlists" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
