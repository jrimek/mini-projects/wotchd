require 'rails_helper'

RSpec.describe "ratings/new", type: :view do
  before(:each) do
    assign(:rating, Rating.new(
      :title => "MyString",
      :review => "MyText",
      :rating => "9.99",
      :user => nil,
      :movie => nil
    ))
  end

  it "renders new rating form" do
    render

    assert_select "form[action=?][method=?]", ratings_path, "post" do

      assert_select "input#rating_title[name=?]", "rating[title]"

      assert_select "textarea#rating_review[name=?]", "rating[review]"

      assert_select "input#rating_rating[name=?]", "rating[rating]"

      assert_select "input#rating_user_id[name=?]", "rating[user_id]"

      assert_select "input#rating_movie_id[name=?]", "rating[movie_id]"
    end
  end
end
