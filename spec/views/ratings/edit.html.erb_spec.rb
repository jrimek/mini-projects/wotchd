require 'rails_helper'

RSpec.describe "ratings/edit", type: :view do
  before(:each) do
    @rating = assign(:rating, Rating.create!(
      :title => "MyString",
      :review => "MyText",
      :rating => "9.99",
      :user => nil,
      :movie => nil
    ))
  end

  it "renders the edit rating form" do
    render

    assert_select "form[action=?][method=?]", rating_path(@rating), "post" do

      assert_select "input#rating_title[name=?]", "rating[title]"

      assert_select "textarea#rating_review[name=?]", "rating[review]"

      assert_select "input#rating_rating[name=?]", "rating[rating]"

      assert_select "input#rating_user_id[name=?]", "rating[user_id]"

      assert_select "input#rating_movie_id[name=?]", "rating[movie_id]"
    end
  end
end
