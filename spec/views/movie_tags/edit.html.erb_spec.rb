require 'rails_helper'

RSpec.describe "movie_tags/edit", type: :view do

  before { skip("TODO") }

  before(:each) do
    @movie_tag = assign(:movie_tag, MovieTag.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit movie_tag form" do
    render

    assert_select "form[action=?][method=?]", movie_tag_path(@movie_tag), "post" do

      assert_select "input#movie_tag_name[name=?]", "movie_tag[name]"
    end
  end
end
