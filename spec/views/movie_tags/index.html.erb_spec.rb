require 'rails_helper'

RSpec.describe "movie_tags/index", type: :view do

  before { skip("TODO") }

  before(:each) do
    assign(:movie_tags, [
      MovieTag.create!(
        :name => "Name"
      ),
      MovieTag.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of movie_tags" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
