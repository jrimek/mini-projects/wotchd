require 'rails_helper'

RSpec.describe "movie_tags/new", type: :view do

  before { skip("TODO") }

  before(:each) do
    assign(:movie_tag, MovieTag.new(
      :name => "MyString"
    ))
  end

  it "renders new movie_tag form" do
    render

    assert_select "form[action=?][method=?]", movie_tags_path, "post" do

      assert_select "input#movie_tag_name[name=?]", "movie_tag[name]"
    end
  end
end
