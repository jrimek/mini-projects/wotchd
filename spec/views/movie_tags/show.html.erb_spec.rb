require 'rails_helper'

RSpec.describe "movie_tags/show", type: :view do

  before { skip("TODO") }

  before(:each) do
    @movie_tag = assign(:movie_tag, MovieTag.create!(
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
  end
end
