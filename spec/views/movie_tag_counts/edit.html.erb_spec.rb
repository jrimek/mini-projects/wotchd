require 'rails_helper'

RSpec.describe "movie_tag_counts/edit", type: :view do

  before { skip("TODO") }

  before(:each) do
    @movie_tag_count = assign(:movie_tag_count, MovieTagCount.create!(
      :movie => nil,
      :movie_tag => nil,
      :count => 1
    ))
  end

  it "renders the edit movie_tag_count form" do
    render

    assert_select "form[action=?][method=?]", movie_tag_count_path(@movie_tag_count), "post" do

      assert_select "input#movie_tag_count_movie_id[name=?]", "movie_tag_count[movie_id]"

      assert_select "input#movie_tag_count_movie_tag_id[name=?]", "movie_tag_count[movie_tag_id]"

      assert_select "input#movie_tag_count_count[name=?]", "movie_tag_count[count]"
    end
  end
end
