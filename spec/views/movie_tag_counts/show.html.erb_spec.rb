require 'rails_helper'

RSpec.describe "movie_tag_counts/show", type: :view do

  before { skip("TODO") }

  before(:each) do
    @movie_tag_count = assign(:movie_tag_count, MovieTagCount.create!(
      :movie => nil,
      :movie_tag => nil,
      :count => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/2/)
  end
end
