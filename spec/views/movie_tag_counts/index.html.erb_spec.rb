require 'rails_helper'

RSpec.describe "movie_tag_counts/index", type: :view do

  before { skip("TODO") }

  before(:each) do
    assign(:movie_tag_counts, [
      MovieTagCount.create!(
        :movie => nil,
        :movie_tag => nil,
        :count => 2
      ),
      MovieTagCount.create!(
        :movie => nil,
        :movie_tag => nil,
        :count => 2
      )
    ])
  end

  it "renders a list of movie_tag_counts" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
