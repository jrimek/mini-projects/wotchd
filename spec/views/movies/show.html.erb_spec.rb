require 'rails_helper'

RSpec.describe "movies/show", type: :view do

  before { skip("TODO") }

  before(:each) do
    @movie = assign(:movie, Movie.create!(
      :ger_title => "Ger Title",
      :engl_title => "Engl Title",
      :fsk => "",
      :duration => 2,
      :imdb_rating => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Ger Title/)
    expect(rendered).to match(/Engl Title/)
    expect(rendered).to match(//)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/9.99/)
  end
end
