require 'rails_helper'

RSpec.describe "movies/edit", type: :view do

  before { skip("TODO") }

  before(:each) do
    @movie = assign(:movie, Movie.create!(
      :ger_title => "MyString",
      :engl_title => "MyString",
      :fsk => "",
      :duration => 1,
      :imdb_rating => "9.99"
    ))
  end

  it "renders the edit movie form" do
    render

    assert_select "form[action=?][method=?]", movie_path(@movie), "post" do

      assert_select "input#movie_ger_title[name=?]", "movie[ger_title]"

      assert_select "input#movie_engl_title[name=?]", "movie[engl_title]"

      assert_select "input#movie_fsk[name=?]", "movie[fsk]"

      assert_select "input#movie_duration[name=?]", "movie[duration]"

      assert_select "input#movie_imdb_rating[name=?]", "movie[imdb_rating]"
    end
  end
end
