require 'rails_helper'

RSpec.describe "movies/index", type: :view do

  before { skip("TODO") }

  before(:each) do
    assign(:movies, [
      Movie.create!(
        :ger_title => "Ger Title",
        :engl_title => "Engl Title",
        :fsk => "",
        :duration => 2,
        :imdb_rating => "9.99"
      ),
      Movie.create!(
        :ger_title => "Ger Title",
        :engl_title => "Engl Title",
        :fsk => "",
        :duration => 2,
        :imdb_rating => "9.99"
      )
    ])
  end

  it "renders a list of movies" do
    render
    assert_select "tr>td", :text => "Ger Title".to_s, :count => 2
    assert_select "tr>td", :text => "Engl Title".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
