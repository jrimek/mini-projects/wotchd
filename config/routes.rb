Rails.application.routes.draw do
  resources :actor_histories
  resources :movie_watchlists
  resources :actor_fanlists
  resources :actor_lists
  resources :movie_histories
  resources :ratings
  resources :movie_lists
  resources :seasons
  resources :age_ratings
  resources :cast_members
  resources :actors
  resources :movie_tag_counts
  resources :movie_tags
  resources :countries
  resources :genres
  devise_for :users
  resources :movies
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
