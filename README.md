# unfinished and abandoned =(

##Installation
`git clone https://github.com/jimbo-on-ruby/wotchd`

`cd wotchd`

`bundle install`

`rails db:setup`

`rails s`

##BRANCHING MODEL

1. _NEVER_ create a PR directly to master!

2. create a new branch for every major change

3. push that branch to github

4. create pull request to development branch

5. let it review by another contributer

6. only the admin merges development into master!

##COMMIT MESSAGES

- Add #ISSUE_NUMBER to the comment section of the commit message

e.g.
```
Update Readme commit instructions

 #25

```

This results in the commit message beeing shown in the issue itself, which
makes it easier to follow the ticket and how it was solved

- follow [those](http://chris.beams.io/posts/git-commit/) guidelines

##CORE PRINCIPLES

 - mobile friendly
 - fast and user friendly UI
 - community driven

##MVP (Minimum Viable Product)

####Movies & TV-Shows (&Animes)

 - rate/review a movie
 - display movie relevant data
 - search
 - display all movies and filter them
 - mobile friendly
 - watchlist / fan of actor
 - ensure proper review
   - title
   - description
   - ...
 - tags
 - display imdb rating
 - social media presence

 - ???

 - movie lists,actor lists
 - login via fb, google etc.
 - discuss a movie
 - online discussion platform
 - User ranking system
 - board / time line / chronic

##CATEGORIES

- Movies
- TV-Series
- Animes
  - Movies
  - TV-Series
  - ...
