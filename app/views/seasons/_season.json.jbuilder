json.extract! season, :id, :number, :name, :release_date, :created_at, :updated_at
json.url season_url(season, format: :json)