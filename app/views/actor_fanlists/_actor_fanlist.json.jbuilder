json.extract! actor_fanlist, :id, :actor_id, :user_id, :created_at, :updated_at
json.url actor_fanlist_url(actor_fanlist, format: :json)