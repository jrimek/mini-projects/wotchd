json.extract! cast_crew_member, :id, :name, :birthday, :created_at, :updated_at
json.url cast_crew_member_url(cast_crew_member, format: :json)