json.extract! movie_history, :id, :movie_id, :movie_content, :created_at, :updated_at
json.url movie_history_url(movie_history, format: :json)