json.extract! age_rating, :id, :created_at, :updated_at
json.url age_rating_url(age_rating, format: :json)