json.extract! actor_list, :id, :name, :fan_list, :user_id, :created_at, :updated_at
json.url actor_list_url(actor_list, format: :json)