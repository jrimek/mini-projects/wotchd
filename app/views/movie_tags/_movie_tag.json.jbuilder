json.extract! movie_tag, :id, :name, :created_at, :updated_at
json.url movie_tag_url(movie_tag, format: :json)