json.extract! actor_history, :id, :actor_id, :actor_content, :created_at, :updated_at
json.url actor_history_url(actor_history, format: :json)