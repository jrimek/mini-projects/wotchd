json.extract! cast_crew, :id, :movie_id, :cast_crew_member_id, :created_at, :updated_at
json.url cast_crew_url(cast_crew, format: :json)