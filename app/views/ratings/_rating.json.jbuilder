json.extract! rating, :id, :title, :review, :rating, :user_id, :movie_id, :created_at, :updated_at
json.url rating_url(rating, format: :json)