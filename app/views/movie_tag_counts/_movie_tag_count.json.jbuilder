json.extract! movie_tag_count, :id, :movie_id, :movie_tag_id, :count, :created_at, :updated_at
json.url movie_tag_count_url(movie_tag_count, format: :json)