json.extract! movie_watchlist, :id, :movie_id, :user_id, :created_at, :updated_at
json.url movie_watchlist_url(movie_watchlist, format: :json)