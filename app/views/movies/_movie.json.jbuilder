json.extract! movie, :id, :ger_title, :engl_title, :fsk, :realease_date, :duration, :imdb_rating, :created_at, :updated_at
json.url movie_url(movie, format: :json)