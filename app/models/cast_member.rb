class CastMember < ApplicationRecord
  belongs_to :movie
  belongs_to :actor

  validates :role, presence: true

  enum role: {
    actor:        'Schauspieler',
    director:     'Regisseur',
    producer:     'Produzent',
    screenwriter: 'Drehbuchautor'
  }
end
