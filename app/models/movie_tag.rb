class MovieTag < ApplicationRecord
  has_many :movie_tag_counts
  has_many :movies, through: :movie_tag_counts

  validates_length_of :name, maximum: 50, allow_blank: false

end
