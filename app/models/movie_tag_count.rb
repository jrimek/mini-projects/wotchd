class MovieTagCount < ApplicationRecord
  belongs_to :movie
  belongs_to :movie_tag

  validates :count, presence: true

  #rewrite valid? doesnt work, probably because of missing .id
end
