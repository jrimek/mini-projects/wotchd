class ActorFanlist < ApplicationRecord
  belongs_to :user
  belongs_to :actor

  # implicit requirement to have fk by rails5
  validates_uniqueness_of :actor_id, scope: [:user_id]
end
