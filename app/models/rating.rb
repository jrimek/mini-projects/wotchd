class Rating < ApplicationRecord
  belongs_to :user
  belongs_to :movie

  validates_length_of :title, maximum: 100, allow_blank: true
  validates :rating, presence: true
  validates :movie_id, presence: true
  validates_uniqueness_of :movie_id, scope: [:user_id]
  validates :user_id, presence: true
end
