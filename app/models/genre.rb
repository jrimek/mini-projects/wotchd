class Genre < ApplicationRecord
  has_and_belongs_to_many :movies

  validates_length_of :name, maximum: 50, allow_blank: false

end
