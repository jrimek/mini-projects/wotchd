class AgeRating < ApplicationRecord
  has_many :movies

  validates :rating, presence: true

  enum rating: {
    fsk0:  'FSK 0',
    fsk6:  'FSK 6',
    fsk12: 'FSK 12',
    fsk16: 'FSK 16',
    fsk18: 'FSK 18',
    pg:    'PG',
    pg13:  'PG-13',
    r:     'R',
    nc17:  'NC-17'
  }

end
