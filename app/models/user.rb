class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :ratings
  # has_many :movies, through: :ratings
    # there is no point in displaying all rated movies from the user class
    # just use tje ratings controller or define custom methods
    # also there is another has_many movies which makes it ambigious
  has_many :movie_watchlists
  # has_many :movies, through: :movie_watchlists
  has_many :movie_lists

  validates_length_of :nickname, maximum: 30, allow_blank: true


end
