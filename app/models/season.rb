class Season < ApplicationRecord
  belongs_to :movie, optional: true
  #movie can be optional, because I plan to realize e.g. movie trilogies as
  #seasons and such a "season" wouldn't belong to a movie
  #unlike "real" seasons, those should always belong to a movie
  #with subdivision = 'TV-Show'
  has_many :movies

  validates :number, presence: true
  validates_length_of :name, maximum: 50, allow_blank: false
end
