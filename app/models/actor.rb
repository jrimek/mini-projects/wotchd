class Actor < ApplicationRecord
  has_and_belongs_to_many :actor_lists
  has_many :cast_members
  has_many :movies, through: :cast_members
  has_many :actor_fanlists
  has_many :users, through: :actor_fanlists

  validates :sex, presence: true
  validates_length_of :first_name, maximum: 50, allow_blank: false
  validates_length_of :last_name, maximum: 50, allow_blank: false
  validates_length_of :middle_name, maximum: 50, allow_blank: true

  enum sex: {
    male:   'Mann',
    female: 'Frau'
  }
end
