class MovieList < ApplicationRecord
  belongs_to :user

  has_and_belongs_to_many :movies do
    def << (movie)
      proxy_association.owner.add_movie(movie)
    end
  end

  validates_length_of :name, maximum: 50, allow_blank: false

  #only add the movie if it is not already in the list
  def add_movie(movie)
    movies.push(movie) if movies.map(&:id).exclude?(movie.id)
  end

end
