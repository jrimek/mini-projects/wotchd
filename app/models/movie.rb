class Movie < ApplicationRecord
  has_many :movie_watchlists
  has_many :users, through: :movie_watchlists
  has_many :ratings
  has_many :users, through: :ratings
  has_and_belongs_to_many :movie_lists
  has_many :seasons
  belongs_to :season, optional: true
  belongs_to :age_rating
  has_and_belongs_to_many :genres
  has_and_belongs_to_many :countries
  has_many :cast_members
  has_many :actors, through: :cast_members
  has_many :movie_tag_counts
  has_many :movie_tags, through: :movie_tag_counts do
    def <<(movie_tag)
      proxy_association.owner.add_movie_tag(movie_tag)
    end
  end

  enum subdivision: {
    movie:   'Movie',
    tv_show: 'TV Show',
    episode: 'Episode'
  }

  validates :release_date, presence: true
  validates :duration, presence: true
  validates :subdivision, presence: true
  validates_length_of :engl_title, maximum: 100, allow_blank: false
  validates_length_of :ger_title, maximum: 100, allow_blank: true

  def add_actor_with_role(actor, role)
    # should extract this condition to an extra method, but can't come up
    # with an explanatory name
    # actor_with_this_role_already_part_of_movie? :D
    # it just checks if the object that we wan't to create already exists
    cast_member = cast_members.find_by(actor_id: actor.id,
                                role:     role)
    if cast_member.nil?
      CastMember.create movie_id: id,
                        actor_id: actor.id,
                        role: role
    end
  end

  def add_movie_tag(movie_tag)
    mtc = movie_tag_counts.find_or_initialize_by(movie_tag_id: movie_tag.id)
    mtc.count ? mtc.count += 1 : mtc.count = 1
    mtc.save
  end

  def self.find_movie_and_its_data(id)
    find_by_sql ["

    WITH rating AS(
    --costs a shitload of resources, query time jumps from 100ms to 1000
    --gotta cache this somehow
      SELECT  r.movie_id,
              round(AVG(r.rating), 1) AS avg_rating
      FROM    ratings AS r
     GROUP BY r.movie_id
    ),

    genre AS(
              SELECT  gem.movie_id,
                      json_agg(gem) AS genres
              FROM    (SELECT  gm.movie_id, g.name
                       FROM    genres AS g
                       JOIN    genres_movies as gm ON gm.genre_id = g.id
                      ) as gem
             GROUP BY gem.movie_id
    ),

    country AS(
    SELECT result.movie_id,
           json_agg(result) AS countries
      FROM (SELECT cm.movie_id, c.name
            FROM   countries AS c
            JOIN   countries_movies as cm ON cm.country_id = c.id
           ) as result
  GROUP BY result.movie_id
    ),

    movie_tag AS(
      SELECT result.movie_id,
             array_agg(result) AS movie_tags
       FROM  (SELECT mtc.movie_id,
                     mt.name,
                     mtc.count
                FROM movie_tags AS mt
                JOIN movie_tag_counts AS mtc ON mtc.movie_tag_id = mt.id) AS result
    GROUP BY result.movie_id
    ),

    actor AS(
      SELECT result.movie_id,
             json_agg(result) AS actors
       FROM  (SELECT cc.movie_id,
                     ccm.first_name,
                     ccm.last_name,
                     cc.role
              FROM   actors AS ccm
              JOIN   cast_members AS cc ON cc.actor_id = ccm.id
            GROUP BY cc.movie_id,
                     ccm.first_name,
                     ccm.last_name,
                     cc.role
                     ) AS result
       GROUP BY result.movie_id
    ),

    age_rating AS(
      SELECT result.movie_id, json_agg(result) AS age_ratings
       FROM  (SELECT arm.movie_id, ar.rating
              FROM   age_ratings AS ar
              JOIN   age_ratings_movies as arm ON arm.age_rating_id = ar.id
              WHERE  ar.region = 'DE'
                     ) AS result
    GROUP BY result.movie_id
    )

    SELECT  m.id,
            m.engl_title,
            m.ger_title,
            m.release_date,
            m.duration,
            m.imdb_rating,
            genre.genres,
            country.countries,
            movie_tag.movie_tags,
            actor.actors,
            age_rating.age_ratings,
            rating.avg_rating
    FROM movies AS m

    LEFT JOIN rating ON rating.movie_id = m.id

    LEFT JOIN genre ON genre.movie_id = m.id

    LEFT JOIN country ON country.movie_id = m.id

    LEFT JOIN movie_tag ON movie_tag.movie_id = m.id

    LEFT JOIN actor ON actor.movie_id = m.id

    LEFT JOIN age_rating ON age_rating.movie_id = m.id

    WHERE m.id = ?

    ORDER BY m.id
    ;", id]
  end
#  if your are not familiar with the WITH clause, check this article
#  about postgres CTEs:
#  http://www.craigkerstiens.com/2013/11/18/best-postgres-feature-youre-not-using/
#  what I did here is basically left joining all movie related tables
#  and bundle the result of those query with json_agg
#
#  the result would look like this
#
#    column          | value
#  engl_title        | Jackass: The Movie
#  ger_title         | Jackass: The Movie
#  release_date      | 2002-02-27
#  duration          | 87
#  imdb_rating       | 6.6
#  genres            | [{"movie_id":1,"name":"Komödie"},
#                    |  {"movie_id":1,"name":"Action"},
#                    |  {"movie_id":1,"name":"genre_4"}]
#  countries         | [{"movie_id":1,"name":"UK"},
#                    |  {"movie_id":1,"name":"country_4"},
#                    |  {"movie_id":1,"name":"country_7"},
#                    |  {"movie_id":1,"name":"country_5"}]
#  movie_tags        | {"(1,\"hirnlose action\",50)","(1,\"zum heulen\",1)",
#                    | "(1,movie_tag_3,1)","(1,movie_tag_0,1)",
#                    | "(1,beste,2)","(1,movie_tag_5,2)","(1,movie_tag_9,2)",
#                    | "(1,movie_tag_2,2)","(1,movie_tag_6,1)","(1,totgelacht,6)",
#                    | "(1,movie_tag_7,1)"}
#  actors | [{"movie_id":1,"first_name":"Steve","last_name":"-O",
#                    | "role":"Schauspieler"},                                                                                                                                                                                                                                                                             +
#                    |  {"movie_id":1,"first_name":"first_name_45",
#                    |  "last_name":"last_name_45","role":"Schauspieler"},                                                                                                                                                                                                                                                           +
#                    |  {"movie_id":1,"first_name":"first_name_1",
#                    |  "last_name":"last_name_1","role":"Schauspieler"}]
#  age_ratings       | [{"movie_id":1,"rating":"FSK 18"}
  def self.all_movies_and_their_data
    find_by_sql("

    WITH rating AS(
    --costs a shitload of resources, query time jumps from 100ms to 1000
    --gotta cache this somehow
      SELECT  r.movie_id,
              round(AVG(r.rating), 1) AS avg_rating
      FROM    ratings AS r
     GROUP BY r.movie_id
    ),

    genre AS(
              SELECT  gem.movie_id,
                      json_agg(gem) AS genres
              FROM    (SELECT  gm.movie_id, g.name
                       FROM    genres AS g
                       JOIN    genres_movies as gm ON gm.genre_id = g.id
                      ) as gem
             GROUP BY gem.movie_id
    ),

    country AS(
    SELECT result.movie_id,
           json_agg(result) AS countries
      FROM (SELECT cm.movie_id, c.name
            FROM   countries AS c
            JOIN   countries_movies as cm ON cm.country_id = c.id
           ) as result
  GROUP BY result.movie_id
    ),

    movie_tag AS(
      SELECT result.movie_id,
             array_agg(result) AS movie_tags
       FROM  (SELECT mtc.movie_id,
                     mt.name,
                     mtc.count
                FROM movie_tags AS mt
                JOIN movie_tag_counts AS mtc ON mtc.movie_tag_id = mt.id) AS result
    GROUP BY result.movie_id
    ),

    actor AS(
      SELECT result.movie_id,
             json_agg(result) AS actors
       FROM  (SELECT cc.movie_id,
                     ccm.first_name,
                     ccm.last_name,
                     cc.role
              FROM   actors AS ccm
              JOIN   cast_members AS cc ON cc.actor_id = ccm.id
            GROUP BY cc.movie_id,
                     ccm.first_name,
                     ccm.last_name,
                     cc.role
                     ) AS result
       GROUP BY result.movie_id
    ),

    age_rating AS(
      SELECT result.movie_id, json_agg(result) AS age_ratings
       FROM  (SELECT arm.movie_id, ar.rating
              FROM   age_ratings AS ar
              JOIN   age_ratings_movies as arm ON arm.age_rating_id = ar.id
              WHERE  ar.region = 'DE'
                     ) AS result
    GROUP BY result.movie_id
    )

    SELECT  m.id,
            m.engl_title,
            m.ger_title,
            m.release_date,
            m.duration,
            m.imdb_rating,
            genre.genres,
            country.countries,
            movie_tag.movie_tags,
            actor.actors,
            age_rating.age_ratings
            --,rating.avg_rating
    FROM movies AS m

    --LEFT JOIN rating ON rating.movie_id = m.id

    LEFT JOIN genre ON genre.movie_id = m.id

    LEFT JOIN country ON country.movie_id = m.id

    LEFT JOIN movie_tag ON movie_tag.movie_id = m.id

    LEFT JOIN actor ON actor.movie_id = m.id

    LEFT JOIN age_rating ON age_rating.movie_id = m.id

    ORDER BY m.id
    --rendering a view with 10.000 movies takes quit some time^^
    LIMIT 30
    ;
    ")
  end
end
