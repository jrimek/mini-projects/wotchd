class ActorFanlistsController < ApplicationController
  before_action :set_actor_fanlist, only: [:show, :edit, :update, :destroy]

  # GET /actor_fanlists
  # GET /actor_fanlists.json
  def index
    @actor_fanlists = ActorFanlist.all
  end

  # GET /actor_fanlists/1
  # GET /actor_fanlists/1.json
  def show
  end

  # GET /actor_fanlists/new
  def new
    @actor_fanlist = ActorFanlist.new
  end

  # GET /actor_fanlists/1/edit
  def edit
  end

  # POST /actor_fanlists
  # POST /actor_fanlists.json
  def create
    @actor_fanlist = ActorFanlist.new(actor_fanlist_params)

    respond_to do |format|
      if @actor_fanlist.save
        format.html { redirect_to @actor_fanlist, notice: 'Actor fanlist was successfully created.' }
        format.json { render :show, status: :created, location: @actor_fanlist }
      else
        format.html { render :new }
        format.json { render json: @actor_fanlist.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /actor_fanlists/1
  # PATCH/PUT /actor_fanlists/1.json
  def update
    respond_to do |format|
      if @actor_fanlist.update(actor_fanlist_params)
        format.html { redirect_to @actor_fanlist, notice: 'Actor fanlist was successfully updated.' }
        format.json { render :show, status: :ok, location: @actor_fanlist }
      else
        format.html { render :edit }
        format.json { render json: @actor_fanlist.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /actor_fanlists/1
  # DELETE /actor_fanlists/1.json
  def destroy
    @actor_fanlist.destroy
    respond_to do |format|
      format.html { redirect_to actor_fanlists_url, notice: 'Actor fanlist was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_actor_fanlist
      @actor_fanlist = ActorFanlist.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def actor_fanlist_params
      params.require(:actor_fanlist).permit(:actor_id, :user_id)
    end
end
