class ActorListsController < ApplicationController
  before_action :set_actor_list, only: [:show, :edit, :update, :destroy]

  # GET /actor_lists
  # GET /actor_lists.json
  def index
    @actor_lists = ActorList.all
  end

  # GET /actor_lists/1
  # GET /actor_lists/1.json
  def show
  end

  # GET /actor_lists/new
  def new
    @actor_list = ActorList.new
  end

  # GET /actor_lists/1/edit
  def edit
  end

  # POST /actor_lists
  # POST /actor_lists.json
  def create
    @actor_list = ActorList.new(actor_list_params)

    respond_to do |format|
      if @actor_list.save
        format.html { redirect_to @actor_list, notice: 'Actor list was successfully created.' }
        format.json { render :show, status: :created, location: @actor_list }
      else
        format.html { render :new }
        format.json { render json: @actor_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /actor_lists/1
  # PATCH/PUT /actor_lists/1.json
  def update
    respond_to do |format|
      if @actor_list.update(actor_list_params)
        format.html { redirect_to @actor_list, notice: 'Actor list was successfully updated.' }
        format.json { render :show, status: :ok, location: @actor_list }
      else
        format.html { render :edit }
        format.json { render json: @actor_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /actor_lists/1
  # DELETE /actor_lists/1.json
  def destroy
    @actor_list.destroy
    respond_to do |format|
      format.html { redirect_to actor_lists_url, notice: 'Actor list was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_actor_list
      @actor_list = ActorList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def actor_list_params
      params.require(:actor_list).permit(:name, :fan_list, :user_id)
    end
end
