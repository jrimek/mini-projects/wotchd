class ActorHistoriesController < ApplicationController
  before_action :set_actor_history, only: [:show, :edit, :update, :destroy]

  # GET /actor_histories
  # GET /actor_histories.json
  def index
    @actor_histories = ActorHistory.all
  end

  # GET /actor_histories/1
  # GET /actor_histories/1.json
  def show
  end

  # GET /actor_histories/new
  def new
    @actor_history = ActorHistory.new
  end

  # GET /actor_histories/1/edit
  def edit
  end

  # POST /actor_histories
  # POST /actor_histories.json
  def create
    @actor_history = ActorHistory.new(actor_history_params)

    respond_to do |format|
      if @actor_history.save
        format.html { redirect_to @actor_history, notice: 'Actor history was successfully created.' }
        format.json { render :show, status: :created, location: @actor_history }
      else
        format.html { render :new }
        format.json { render json: @actor_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /actor_histories/1
  # PATCH/PUT /actor_histories/1.json
  def update
    respond_to do |format|
      if @actor_history.update(actor_history_params)
        format.html { redirect_to @actor_history, notice: 'Actor history was successfully updated.' }
        format.json { render :show, status: :ok, location: @actor_history }
      else
        format.html { render :edit }
        format.json { render json: @actor_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /actor_histories/1
  # DELETE /actor_histories/1.json
  def destroy
    @actor_history.destroy
    respond_to do |format|
      format.html { redirect_to actor_histories_url, notice: 'Actor history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_actor_history
      @actor_history = ActorHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def actor_history_params
      params.require(:actor_history).permit(:actor_id, :actor_content)
    end
end
