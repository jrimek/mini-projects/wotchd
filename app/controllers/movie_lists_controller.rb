class MovieListsController < ApplicationController
  before_action :set_movie_list, only: [:show, :edit, :update, :destroy]

  # GET /movie_lists
  # GET /movie_lists.json
  def index
    @movie_lists = MovieList.all
  end

  # GET /movie_lists/1
  # GET /movie_lists/1.json
  def show
  end

  # GET /movie_lists/new
  def new
    @movie_list = MovieList.new
  end

  # GET /movie_lists/1/edit
  def edit
  end

  # POST /movie_lists
  # POST /movie_lists.json
  def create
    @movie_list = MovieList.new(movie_list_params)

    respond_to do |format|
      if @movie_list.save
        format.html { redirect_to @movie_list, notice: 'MovieList was successfully created.' }
        format.json { render :show, status: :created, location: @movie_list }
      else
        format.html { render :new }
        format.json { render json: @movie_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /movie_lists/1
  # PATCH/PUT /movie_lists/1.json
  def update
    respond_to do |format|
      if @movie_list.update(movie_list_params)
        format.html { redirect_to @movie_list, notice: 'MovieList was successfully updated.' }
        format.json { render :show, status: :ok, location: @movie_list }
      else
        format.html { render :edit }
        format.json { render json: @movie_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movie_lists/1
  # DELETE /movie_lists/1.json
  def destroy
    @movie_list.destroy
    respond_to do |format|
      format.html { redirect_to movie_lists_url, notice: 'MovieList was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movie_list
      @movie_list = MovieList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white movie_list through.
    def movie_list_params
      params.require(:movie_list).permit(:name, :watchmovie_list, :user_id)
    end
end
