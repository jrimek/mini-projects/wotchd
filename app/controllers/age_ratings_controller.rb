class AgeRatingsController < ApplicationController
  before_action :set_age_rating, only: [:show, :edit, :update, :destroy]

  # GET /age_ratings
  # GET /age_ratings.json
  def index
    @age_ratings = AgeRating.all
  end

  # GET /age_ratings/1
  # GET /age_ratings/1.json
  def show
  end

  # GET /age_ratings/new
  def new
    @age_rating = AgeRating.new
  end

  # GET /age_ratings/1/edit
  def edit
  end

  # POST /age_ratings
  # POST /age_ratings.json
  def create
    @age_rating = AgeRating.new(age_rating_params)

    respond_to do |format|
      if @age_rating.save
        format.html { redirect_to @age_rating, notice: 'Age rating was successfully created.' }
        format.json { render :show, status: :created, location: @age_rating }
      else
        format.html { render :new }
        format.json { render json: @age_rating.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /age_ratings/1
  # PATCH/PUT /age_ratings/1.json
  def update
    respond_to do |format|
      if @age_rating.update(age_rating_params)
        format.html { redirect_to @age_rating, notice: 'Age rating was successfully updated.' }
        format.json { render :show, status: :ok, location: @age_rating }
      else
        format.html { render :edit }
        format.json { render json: @age_rating.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /age_ratings/1
  # DELETE /age_ratings/1.json
  def destroy
    @age_rating.destroy
    respond_to do |format|
      format.html { redirect_to age_ratings_url, notice: 'Age rating was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_age_rating
      @age_rating = AgeRating.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def age_rating_params
      params.fetch(:age_rating, {})
    end
end
