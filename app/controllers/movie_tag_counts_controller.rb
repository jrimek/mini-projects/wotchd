class MovieTagCountsController < ApplicationController
  before_action :set_movie_tag_count, only: [:show, :edit, :update, :destroy]

  # GET /movie_tag_counts
  # GET /movie_tag_counts.json
  def index
    @movie_tag_counts = MovieTagCount.all
  end

  # GET /movie_tag_counts/1
  # GET /movie_tag_counts/1.json
  def show
  end

  # GET /movie_tag_counts/new
  def new
    @movie_tag_count = MovieTagCount.new
  end

  # GET /movie_tag_counts/1/edit
  def edit
  end

  # POST /movie_tag_counts
  # POST /movie_tag_counts.json
  def create
    @movie_tag_count = MovieTagCount.new(movie_tag_count_params)

    respond_to do |format|
      if @movie_tag_count.save
        format.html { redirect_to @movie_tag_count, notice: 'Movie tag count was successfully created.' }
        format.json { render :show, status: :created, location: @movie_tag_count }
      else
        format.html { render :new }
        format.json { render json: @movie_tag_count.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /movie_tag_counts/1
  # PATCH/PUT /movie_tag_counts/1.json
  def update
    respond_to do |format|
      if @movie_tag_count.update(movie_tag_count_params)
        format.html { redirect_to @movie_tag_count, notice: 'Movie tag count was successfully updated.' }
        format.json { render :show, status: :ok, location: @movie_tag_count }
      else
        format.html { render :edit }
        format.json { render json: @movie_tag_count.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movie_tag_counts/1
  # DELETE /movie_tag_counts/1.json
  def destroy
    @movie_tag_count.destroy
    respond_to do |format|
      format.html { redirect_to movie_tag_counts_url, notice: 'Movie tag count was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movie_tag_count
      @movie_tag_count = MovieTagCount.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def movie_tag_count_params
      params.require(:movie_tag_count).permit(:movie_id, :movie_tag_id, :count)
    end
end
