class MovieWatchlistsController < ApplicationController
  before_action :set_movie_watchlist, only: [:show, :edit, :update, :destroy]

  # GET /movie_watchlists
  # GET /movie_watchlists.json
  def index
    @movie_watchlists = MovieWatchlist.all
  end

  # GET /movie_watchlists/1
  # GET /movie_watchlists/1.json
  def show
  end

  # GET /movie_watchlists/new
  def new
    @movie_watchlist = MovieWatchlist.new
  end

  # GET /movie_watchlists/1/edit
  def edit
  end

  # POST /movie_watchlists
  # POST /movie_watchlists.json
  def create
    @movie_watchlist = MovieWatchlist.new(movie_watchlist_params)

    respond_to do |format|
      if @movie_watchlist.save
        format.html { redirect_to @movie_watchlist, notice: 'Movie watchlist was successfully created.' }
        format.json { render :show, status: :created, location: @movie_watchlist }
      else
        format.html { render :new }
        format.json { render json: @movie_watchlist.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /movie_watchlists/1
  # PATCH/PUT /movie_watchlists/1.json
  def update
    respond_to do |format|
      if @movie_watchlist.update(movie_watchlist_params)
        format.html { redirect_to @movie_watchlist, notice: 'Movie watchlist was successfully updated.' }
        format.json { render :show, status: :ok, location: @movie_watchlist }
      else
        format.html { render :edit }
        format.json { render json: @movie_watchlist.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movie_watchlists/1
  # DELETE /movie_watchlists/1.json
  def destroy
    @movie_watchlist.destroy
    respond_to do |format|
      format.html { redirect_to movie_watchlists_url, notice: 'Movie watchlist was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movie_watchlist
      @movie_watchlist = MovieWatchlist.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def movie_watchlist_params
      params.require(:movie_watchlist).permit(:movie_id, :user_id)
    end
end
