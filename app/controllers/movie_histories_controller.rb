class MovieHistoriesController < ApplicationController
  before_action :set_movie_history, only: [:show, :edit, :update, :destroy]

  # GET /movie_histories
  # GET /movie_histories.json
  def index
    @movie_histories = MovieHistory.all
  end

  # GET /movie_histories/1
  # GET /movie_histories/1.json
  def show
  end

  # GET /movie_histories/new
  def new
    @movie_history = MovieHistory.new
  end

  # GET /movie_histories/1/edit
  def edit
  end

  # POST /movie_histories
  # POST /movie_histories.json
  def create
    @movie_history = MovieHistory.new(movie_history_params)

    respond_to do |format|
      if @movie_history.save
        format.html { redirect_to @movie_history, notice: 'Movie history was successfully created.' }
        format.json { render :show, status: :created, location: @movie_history }
      else
        format.html { render :new }
        format.json { render json: @movie_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /movie_histories/1
  # PATCH/PUT /movie_histories/1.json
  def update
    respond_to do |format|
      if @movie_history.update(movie_history_params)
        format.html { redirect_to @movie_history, notice: 'Movie history was successfully updated.' }
        format.json { render :show, status: :ok, location: @movie_history }
      else
        format.html { render :edit }
        format.json { render json: @movie_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movie_histories/1
  # DELETE /movie_histories/1.json
  def destroy
    @movie_history.destroy
    respond_to do |format|
      format.html { redirect_to movie_histories_url, notice: 'Movie history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movie_history
      @movie_history = MovieHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def movie_history_params
      params.require(:movie_history).permit(:movie_id, :movie_content)
    end
end
